/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.repository;

import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.utils.Utils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentsRepository extends BaseRepository<Comment> {
    private static final Logger log = LoggerFactory.getLogger(CommentsRepository.class);

    public Resource getIncidentIdForComment(String commentId) {
        final Resource[] incidentResource = {null};

        Repositories.consume(super.repositoryConnector.getRepository(), conn -> {
            TupleQueryResult result = conn.prepareTupleQuery(
                    MZ_SPARQL_PREFIX + "SELECT ?incident WHERE {"
                            + "?incident mz:incidentComments _:blankNode . "
                            + "_:blankNode ?inSequence <http://mapazlocinu.opendata.sk/comment/" + commentId + ">"
                            + "} LIMIT 1"
            ).evaluate();

            while (result.hasNext()) {
                Value value = result.next().getValue("incident");
                if (value != null) {
                    incidentResource[0] = SimpleValueFactory.getInstance().createIRI(value.stringValue());
                }
            }
        });

        return incidentResource[0];
    }

    public int getItemsCountByVisibility(Visibility visibility) {
        String query = MZ_SPARQL_PREFIX
                + "SELECT (COUNT(DISTINCT ?item) AS ?count) WHERE {"
                + "?item a " + getRDFBeanAnnotationValue() + ". "
                + "?item mz:itemVisibility \"" + visibility.getValue() + "\". "
                + "}";

        return executeItemsCountQuery(query);
    }

    public List<Comment> getItemsByVisibility(Visibility visibility, long maxSize) {
        List<Comment> comments = new ArrayList<>();

        Repositories.consume(repositoryConnector.getRepository(), conn -> {
            TupleQueryResult result = conn.prepareTupleQuery(
                    MZ_SPARQL_PREFIX + SCHEMA_SPARQL_PREFIX + DC_TERMS_SPARQL_PREFIX + FOAF_SPARQL_PREFIX
                            + "SELECT ?item ?text ?createdDate ?type ?author ?authorName ?authorRating ?authorVisible WHERE {"
                            + "?item a " + getRDFBeanAnnotationValue() + ". "
                            + "?item mz:commentContent ?text. "
                            + "?item dcterms:created ?createdDate. "
                            + "?item mz:commentType ?type. "
                            + "?item mz:itemVisibility \"" + visibility.getValue() + "\". "
                            + "?item mz:authorVisible ?authorVisible. "

                            + "?item schema:author ?author. "
                            + "?author foaf:name ?authorName. "
                            + "?author mz:userRating ?authorRating. "

                            + "} LIMIT " + maxSize
            ).evaluate();

            while (result.hasNext()) {
                BindingSet bindings = result.next();

                try {
                    Comment comment = new Comment();
                    comment.setId(bindings.getValue("item").stringValue()
                            .replace("http://mapazlocinu.opendata.sk/comment/", ""));
                    comment.setText(bindings.getValue("text").stringValue());
                    comment.setCreatedDate(Utils.parseDateFromDB(bindings.getValue("createdDate").stringValue()));
                    comment.setVisibility(visibility.getValue());
                    comment.setType(bindings.getValue("type").stringValue());
                    comment.setAuthorVisible(Boolean.valueOf(bindings.getValue("authorVisible").stringValue()));

                    User user = new User();
                    user.setId(bindings.getValue("author").stringValue()
                            .replace("http://mapazlocinu.opendata.sk/user/", ""));
                    user.setName(bindings.getValue("authorName").stringValue());
                    user.setRating(Utils.parseNumberFromString(bindings.getValue("authorRating").stringValue()));

                    comment.setAuthor(user);
                    comments.add(comment);
                } catch (NullPointerException e) {
                    log.error("Unexpected error occurred while parsing result of query.", e);
                }
            }
        });

        return comments;
    }
}
