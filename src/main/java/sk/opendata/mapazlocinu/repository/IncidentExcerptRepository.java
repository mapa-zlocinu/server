/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.repository;

import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.utils.Utils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class IncidentExcerptRepository extends BaseRepository<IncidentExcerpt> {
    private static final Logger log = LoggerFactory.getLogger(IncidentExcerptRepository.class);

    public int getItemsCountByType(IncidentType incidentType) {
        String query = MZ_SPARQL_PREFIX
                + "SELECT (COUNT(DISTINCT ?item) AS ?count) WHERE {"
                + "?item a " + getRDFBeanAnnotationValue() + ". "
                + "?item mz:incidentType \"" + incidentType.getValue() + "\". "
                + "}";

        return executeItemsCountQuery(query);
    }

    public int getItemsCountByVisibility(Visibility visibility) {
        String query = MZ_SPARQL_PREFIX
                + "SELECT (COUNT(DISTINCT ?item) AS ?count) WHERE {"
                + "?item a " + getRDFBeanAnnotationValue() + ". "
                + "?item mz:itemVisibility \"" + visibility.getValue() + "\". "
                + "}";

        return executeItemsCountQuery(query);
    }

    public List<IncidentExcerpt> getItemsByVisibility(Visibility visibility, long maxSize) {
        List<IncidentExcerpt> incidents = new ArrayList<>();

        Repositories.consume(repositoryConnector.getRepository(), conn -> {
            TupleQueryResult result = conn.prepareTupleQuery(
                    MZ_SPARQL_PREFIX + SCHEMA_SPARQL_PREFIX + DC_TERMS_SPARQL_PREFIX
                            + "SELECT ?item ?title ?incidentDate ?lat ?lon ?type WHERE {"
                            + "?item a " + getRDFBeanAnnotationValue() + ". "
                            + "?item dcterms:title ?title. "
                            + "?item schema:latitude ?lat. "
                            + "?item schema:longitude ?lon. "
                            + "?item mz:incidentType ?type. "
                            + "?item mz:incidentDate ?incidentDate. "
                            + "?item mz:itemVisibility \"" + visibility.getValue() + "\". "
                            + "} LIMIT " + maxSize
            ).evaluate();

            while (result.hasNext()) {
                BindingSet bindings = result.next();

                try {
                    IncidentExcerpt incident = new IncidentExcerpt();

                    incident.setId(bindings.getValue("item").stringValue()
                            .replace("http://mapazlocinu.opendata.sk/incident/", ""));
                    incident.setTitle(bindings.getValue("title").stringValue());
                    incident.setLat(Utils.parseNumberFromStringToDouble(bindings.getValue("lat").stringValue()));
                    incident.setLon(Utils.parseNumberFromStringToDouble(bindings.getValue("lon").stringValue()));
                    incident.setType(bindings.getValue("type").stringValue());
                    incident.setIncidentDate(Utils.parseDateFromDB(bindings.getValue("incidentDate").stringValue()));
                    incident.setVisibility(visibility.getValue());

                    incidents.add(incident);
                } catch (NullPointerException e) {
                    log.error("Unexpected error occurred while parsing result of query.", e);
                }
            }
        });

        return incidents;
    }
}
