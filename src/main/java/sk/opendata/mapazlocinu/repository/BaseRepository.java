/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.repository;

import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;
import sk.opendata.mapazlocinu.config.RdfRepositoryConnector;
import sk.opendata.mapazlocinu.exception.RDFBeanRuntimeException;
import sk.opendata.mapazlocinu.utils.Utils;

import java.util.ArrayList;
import java.util.List;

@Component
public abstract class BaseRepository<T> {
    static final String MZ_SPARQL_PREFIX = "PREFIX mz: <http://mapazlocinu.opendata.sk/> \n";
    static final String SCHEMA_SPARQL_PREFIX = "PREFIX schema: <http://schema.org/> \n";
    static final String DC_TERMS_SPARQL_PREFIX = "PREFIX dcterms: <http://purl.org/dc/terms/> \n";
    static final String FOAF_SPARQL_PREFIX = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n";

    private static final Logger log = LoggerFactory.getLogger(BaseRepository.class);

    private final Class<T> type;

    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    @Autowired
    RdfRepositoryConnector repositoryConnector;

    @SuppressWarnings("unchecked")
    public BaseRepository() {
        final Class<?>[] classes = GenericTypeResolver.resolveTypeArguments(getClass(), BaseRepository.class);
        this.type = (Class<T>) classes[0];
    }

    public T findById(String id) {
        try {
            return repositoryConnector.getRDFBeanManager().get(id, type);
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public T findById(Resource resource) {
        try {
            return repositoryConnector.getRDFBeanManager().get(resource, type);
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public List<T> findAll() {
        try {
            List<T> all = new ArrayList<>();
            Utils.iterableToCollection(repositoryConnector.getRDFBeanManager().getAll(type), all);
            return all;
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public void save(T t) {
        try {
            repositoryConnector.getRDFBeanManager().add(t);
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public void update(T t) {
        try {
            repositoryConnector.getRDFBeanManager().update(t);
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public void delete(String id) {
        try {
            repositoryConnector.getRDFBeanManager().delete(id, type);
        } catch (RDFBeanException | RDF4JException e) {
            throw new RDFBeanRuntimeException(e.getMessage());
        }
    }

    public int getItemsCount() {
        String query = MZ_SPARQL_PREFIX
                + "SELECT (COUNT(DISTINCT ?item) AS ?count) WHERE {"
                + "?item a " + getRDFBeanAnnotationValue() + ". "
                + "}";

        return executeItemsCountQuery(query);
    }

    String getRDFBeanAnnotationValue() {
        try {
            return this.type.getAnnotation(RDFBean.class).value();
        } catch (NullPointerException e) {
            log.error("Error occurred while value of RDFBean annotation from class " + this.type.getName()
                    + " was read.", e);
        }

        return "";
    }

    int executeItemsCountQuery(String sparqlQuery) {
        final int[] itemsCount = {0};

        Repositories.consume(repositoryConnector.getRepository(), conn -> {
            TupleQueryResult result = conn.prepareTupleQuery(sparqlQuery).evaluate();

            while (result.hasNext()) {
                Value value = result.next().getValue("count");
                if (value != null) {
                    itemsCount[0] = Utils.parseNumberFromString(value.stringValue());
                }
            }
        });

        return itemsCount[0];
    }
}
