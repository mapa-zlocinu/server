/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.repository;

import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.springframework.stereotype.Repository;
import sk.opendata.mapazlocinu.entity.Attachment;

@Repository
public class AttachmentRepository extends BaseRepository<Attachment> {

    public Resource getIncidentIdForAttachment(String attachmentId) {
        final Resource[] incidentResource = {null};

        Repositories.consume(super.repositoryConnector.getRepository(), conn -> {
            TupleQueryResult result = conn.prepareTupleQuery(
                    MZ_SPARQL_PREFIX + "SELECT ?incident WHERE {"
                            + "?incident mz:incidentAttachments _:blankNode . "
                            + "_:blankNode ?inSequence <http://mapazlocinu.opendata.sk/attachment/" + attachmentId + ">"
                            + "} LIMIT 1"
            ).evaluate();

            while (result.hasNext()) {
                Value value = result.next().getValue("incident");
                if (value != null) {
                    incidentResource[0] = SimpleValueFactory.getInstance().createIRI(value.stringValue());
                }
            }
        });

        return incidentResource[0];
    }
}
