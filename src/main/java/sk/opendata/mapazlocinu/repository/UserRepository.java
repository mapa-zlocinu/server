/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.repository;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.springframework.stereotype.Repository;
import sk.opendata.mapazlocinu.entity.User;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository extends BaseRepository<User> {

    public List<String> getUsersIncidentsIds(String userId) {
        return evaluateUserAuthorQuery(userId, "Incident");
    }

    public List<String> getUsersCommentsIds(String userId) {
        return evaluateUserAuthorQuery(userId, "Comment");
    }

    private List<String> evaluateUserAuthorQuery(String userId, String entity) {
        List<String> itemsIds = new ArrayList<>();

        Repositories.consume(super.repositoryConnector.getRepository(), conn -> {
            String prefix = MZ_SPARQL_PREFIX + SCHEMA_SPARQL_PREFIX;

            TupleQueryResult result = conn.prepareTupleQuery(
                    prefix + "SELECT ?item WHERE {"
                            + "?item schema:author <http://mapazlocinu.opendata.sk/user/" + userId + ">.\n"
                            + "?item <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
                            + " <http://mapazlocinu.opendata.sk/" + entity + ">"
                            + "}"
            ).evaluate();

            while (result.hasNext()) {
                Value value = result.next().getValue("item");
                if (value != null) {
                    itemsIds.add(SimpleValueFactory.getInstance().createIRI(value.stringValue()).getLocalName());
                }
            }
        });

        return itemsIds;
    }
}
