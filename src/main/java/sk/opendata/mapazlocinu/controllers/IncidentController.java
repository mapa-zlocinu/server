/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentPublicDto;
import sk.opendata.mapazlocinu.dto.comment.CommentPublicDto;
import sk.opendata.mapazlocinu.dto.comment.CreateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.incident.CreateIncidentRequestDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicExcerptDto;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.services.AttachmentService;
import sk.opendata.mapazlocinu.services.CommentService;
import sk.opendata.mapazlocinu.services.IncidentService;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/incidents")
public class IncidentController {
    private final IncidentService incidentService;
    private final CommentService commentService;
    private final AttachmentService attachmentService;

    @Autowired
    public IncidentController(IncidentService incidentService,
            CommentService commentService,
            AttachmentService attachmentService) {
        this.incidentService = incidentService;
        this.commentService = commentService;
        this.attachmentService = attachmentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<IncidentPublicExcerptDto>> getAllIncidents(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);
        return new ResponseEntity<>(incidentService.getAllPublicIncidents(maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CreateItemResponseDto> createNewIncident(@RequestBody @Valid CreateIncidentRequestDto incident) {
        return new ResponseEntity<>(incidentService.createIncident(incident), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.GET)
    public ResponseEntity<IncidentPublicDto> getIncidentDetail(@PathVariable String incidentId) {
        return new ResponseEntity<>(incidentService.getPublicIncidentDetail(incidentId), HttpStatus.OK);
    }

    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/comments", method = RequestMethod.GET)
    public ResponseEntity<List<CommentPublicDto>> getIncidentComments(@PathVariable String incidentId,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Comment> comparator = Utils.parseCommentSortingType(sortBy, sortType);
        return new ResponseEntity<>(commentService.getIncidentApprovedComments(incidentId, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/comments", method = RequestMethod.POST)
    public ResponseEntity<CreateItemResponseDto> createNewComment(@RequestBody @Valid CreateCommentRequestDto comment,
            @PathVariable String incidentId) {
        return new ResponseEntity<>(commentService.createComment(incidentId, comment), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/attachments", method = RequestMethod.POST)
    public ResponseEntity<CreateItemResponseDto> createNewAttachment(@PathVariable String incidentId,
            @RequestPart MultipartFile file) throws IOException {
        return new ResponseEntity<>(attachmentService.createAttachment(incidentId, file), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/attachments", method = RequestMethod.GET)
    public ResponseEntity<List<AttachmentPublicDto>> getIncidentAttachmentsList(@PathVariable String incidentId,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Attachment> comparator = Utils.parseAttachmentSortingType(sortBy, sortType);
        return new ResponseEntity<>(attachmentService.getApprovedIncidentAttachments(incidentId, maxSize, comparator),
                HttpStatus.OK);
    }

    @SuppressWarnings("ConstantConditions")
    @RequestMapping(path = "/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.GET)
    public ResponseEntity<FileSystemResource> getIncidentAttachmentData(@PathVariable String incidentId,
            @PathVariable String attachmentId, HttpServletResponse response) {

        Attachment attachment = attachmentService.getApprovedIncidentAttachment(incidentId, attachmentId);
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + attachment.getTitle() + attachment.getExtension() + "\"");
        return new ResponseEntity<>(attachmentService.getAttachmentData(attachment), HttpStatus.OK);
    }

    @RequestMapping(path = "/filter/{upperLeftLat}/{upperLeftLon}/{bottomRightLat}/{bottomRightLon}", method = RequestMethod.GET)
    public ResponseEntity<List<IncidentPublicExcerptDto>> getAllIncidentsInBoundary(@PathVariable double upperLeftLat,
            @PathVariable double upperLeftLon,
            @PathVariable double bottomRightLat,
            @PathVariable double bottomRightLon,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);
        return new ResponseEntity<>(incidentService
                .getAllPublicIncidentsInBoundary(upperLeftLat, upperLeftLon, bottomRightLat, bottomRightLon, maxSize,
                        comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/filter/{type}", method = RequestMethod.GET)
    public ResponseEntity<?> getAllIncidentsByType(@PathVariable String type,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        IncidentType typeEnum = Utils.processIncidentTypeFromString(type);
        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);

        return new ResponseEntity<>(incidentService.getAllPublicIncidentsByType(typeEnum, maxSize, comparator),
                HttpStatus.OK);
    }
}
