/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.opendata.mapazlocinu.dto.security.LoginRequest;
import sk.opendata.mapazlocinu.dto.security.LoginResponse;
import sk.opendata.mapazlocinu.security.services.AdminAuthenticationService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin/auth")
public class AuthenticationController {
    private final AdminAuthenticationService adminAuthenticationService;

    @Autowired
    public AuthenticationController(AdminAuthenticationService adminAuthenticationService) {
        this.adminAuthenticationService = adminAuthenticationService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> attemptToSignIn(@RequestBody @Valid LoginRequest request) {
        return new ResponseEntity<>(adminAuthenticationService.authenticateAdmin(request), HttpStatus.OK);
    }
}
