/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.controllers;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api")
public class RootController {

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ResourceSupport> getRootInfo() {
        ResourceSupport indexLinks = new ResourceSupport();

        indexLinks.add(linkTo(RootController.class).withSelfRel());
        indexLinks.add(linkTo(methodOn(IncidentController.class).getAllIncidents(2500L, "date", "asc"))
                .withRel("incidents"));
        indexLinks.add(linkTo(methodOn(IncidentController.class).getAllIncidentsByType("economic",
                2500L, "date", "asc")).withRel("incidents-by-type"));
        indexLinks.add(linkTo(methodOn(IncidentController.class).getAllIncidentsInBoundary(49.12, 12.15, 46.3, 21.3,
                2500L, "date", "asc")).withRel("incidents-in-square-boundary"));

        return new ResponseEntity<>(indexLinks, HttpStatus.OK);
    }
}
