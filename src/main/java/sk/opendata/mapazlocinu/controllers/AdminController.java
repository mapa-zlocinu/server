/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sk.opendata.mapazlocinu.dto.DataSummaryResponseDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentAdminDto;
import sk.opendata.mapazlocinu.dto.attachment.UpdateAttachmentRequestDto;
import sk.opendata.mapazlocinu.dto.comment.CommentAdminDto;
import sk.opendata.mapazlocinu.dto.comment.UpdateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminExcerptDto;
import sk.opendata.mapazlocinu.dto.incident.UpdateIncidentRequestDto;
import sk.opendata.mapazlocinu.dto.user.UpdateUserRequestDto;
import sk.opendata.mapazlocinu.dto.user.UserAdminLinksDto;
import sk.opendata.mapazlocinu.dto.user.UserItemsAuthorResponseDto;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.services.AdminService;
import sk.opendata.mapazlocinu.services.AttachmentService;
import sk.opendata.mapazlocinu.services.CommentService;
import sk.opendata.mapazlocinu.services.IncidentService;
import sk.opendata.mapazlocinu.services.UserService;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    private final AdminService adminService;
    private final IncidentService incidentService;
    private final CommentService commentService;
    private final AttachmentService attachmentService;
    private final UserService userService;

    @Autowired
    public AdminController(AdminService adminService,
            IncidentService incidentService,
            CommentService commentService,
            AttachmentService attachmentService, UserService userService) {
        this.adminService = adminService;
        this.incidentService = incidentService;
        this.commentService = commentService;
        this.attachmentService = attachmentService;
        this.userService = userService;
    }

    @RequestMapping(path = "/summary", method = RequestMethod.GET)
    public ResponseEntity<DataSummaryResponseDto> getDataSummary() {
        return new ResponseEntity<>(adminService.createDataSummary(), HttpStatus.OK);
    }

    /* - INCIDENTS ADMIN API - */
    @RequestMapping(path = "/incidents", method = RequestMethod.GET)
    public ResponseEntity<List<IncidentAdminExcerptDto>> getAllIncidents(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);
        return new ResponseEntity<>(incidentService.getAllIncidentsExcerpt(maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/incidents/pending", method = RequestMethod.GET)
    public ResponseEntity<List<IncidentAdminExcerptDto>> getAllPendingIncidents(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);
        return new ResponseEntity<>(
                incidentService.getAllIncidentsExcerptByVisibility(Visibility.PENDING, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/incidents/disapproved", method = RequestMethod.GET)
    public ResponseEntity<List<IncidentAdminExcerptDto>> getAllDisapprovedIncidents(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<IncidentExcerpt> comparator = Utils.parseIncidentSortingType(sortBy, sortType);
        return new ResponseEntity<>(
                incidentService.getAllIncidentsExcerptByVisibility(Visibility.DISAPPROVED, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.GET)
    public ResponseEntity<IncidentAdminDto> getIncidentDetail(@PathVariable String incidentId) {
        return new ResponseEntity<>(incidentService.getIncidentDetail(incidentId), HttpStatus.OK);
    }

    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.PATCH)
    public ResponseEntity<IncidentAdminDto> updateIncident(@PathVariable String incidentId, @RequestBody @Valid
            UpdateIncidentRequestDto incidentUpdate) {
        return new ResponseEntity<>(incidentService.updateIncident(incidentId, incidentUpdate), HttpStatus.OK);
    }

    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteIncident(@PathVariable String incidentId) {
        incidentService.deleteIncident(incidentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/{visibility}", method = RequestMethod.PATCH)
    public ResponseEntity<IncidentAdminDto> updateIncidentVisibility(@PathVariable String incidentId,
            @PathVariable String visibility) {
        Visibility visibilityEnum = Utils.processVisibilityFromString(visibility);
        return new ResponseEntity<>(incidentService.updateIncidentVisibility(incidentId, visibilityEnum),
                HttpStatus.OK);
    }

    /* - COMMENTS ADMIN API - */
    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/comments", method = RequestMethod.GET)
    public ResponseEntity<List<CommentAdminDto>> getAllCommentsForIncident(@PathVariable String incidentId,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Comment> comparator = Utils.parseCommentSortingType(sortBy, sortType);
        return new ResponseEntity<>(commentService.getIncidentComments(incidentId, maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/comments", method = RequestMethod.GET)
    public ResponseEntity<List<CommentAdminDto>> getAllComments(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Comment> comparator = Utils.parseCommentSortingType(sortBy, sortType);
        return new ResponseEntity<>(commentService.getAllComments(maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/pending", method = RequestMethod.GET)
    public ResponseEntity<List<CommentAdminDto>> getAllPendingComments(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Comment> comparator = Utils.parseCommentSortingType(sortBy, sortType);
        return new ResponseEntity<>(commentService.getCommentsByVisibility(Visibility.PENDING, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/disapproved", method = RequestMethod.GET)
    public ResponseEntity<List<CommentAdminDto>> getAllDisapprovedComments(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Comment> comparator = Utils.parseCommentSortingType(sortBy, sortType);
        return new ResponseEntity<>(commentService.getCommentsByVisibility(Visibility.DISAPPROVED, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/{commentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.GET)
    public ResponseEntity<CommentAdminDto> getComment(@PathVariable String commentId) {
        return new ResponseEntity<>(commentService.getComment(commentId), HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/{commentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/incident", method = RequestMethod.GET)
    public ResponseEntity<IncidentAdminDto> getIncidentForComment(@PathVariable String commentId) {
        return new ResponseEntity<>(commentService.getIncidentForComment(commentId), HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/{commentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.PATCH)
    public ResponseEntity<CommentAdminDto> updateComment(@RequestBody @Valid UpdateCommentRequestDto updateComment,
            @PathVariable String commentId) {
        return new ResponseEntity<>(commentService.updateComment(commentId, updateComment), HttpStatus.OK);
    }

    @RequestMapping(path = "/comments/{commentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteComment(@PathVariable String commentId) {
        commentService.deleteComment(commentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "/comments/{commentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/{visibility}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateCommentVisibility(@PathVariable String commentId, @PathVariable String visibility) {
        Visibility visibilityEnum = Utils.processVisibilityFromString(visibility);
        return new ResponseEntity<>(commentService.updateCommentVisibility(commentId, visibilityEnum), HttpStatus.OK);
    }

    /* - ATTACHMENTS ADMIN API - */
    @RequestMapping(path = "/incidents/{incidentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/attachments", method = RequestMethod.GET)
    public ResponseEntity<List<AttachmentAdminDto>> getIncidentAttachments(@PathVariable String incidentId,
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Attachment> comparator = Utils.parseAttachmentSortingType(sortBy, sortType);
        return new ResponseEntity<>(attachmentService.getIncidentAttachments(incidentId, maxSize, comparator),
                HttpStatus.OK);
    }

    @RequestMapping(path = "/attachments", method = RequestMethod.GET)
    public ResponseEntity<List<AttachmentAdminDto>> getAllAttachments(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "date", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<Attachment> comparator = Utils.parseAttachmentSortingType(sortBy, sortType);
        return new ResponseEntity<>(attachmentService.getAllAttachments(maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.GET)
    public ResponseEntity<AttachmentAdminDto> getAttachment(@PathVariable String attachmentId) {
        return new ResponseEntity<>(attachmentService.getAttachmentWithLinks(attachmentId), HttpStatus.OK);
    }

    @RequestMapping(path = "/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/incident", method = RequestMethod.GET)
    public ResponseEntity<IncidentAdminDto> getIncidentForAttachment(@PathVariable String attachmentId) {
        return new ResponseEntity<>(attachmentService.getIncidentForAttachment(attachmentId), HttpStatus.OK);
    }

    @SuppressWarnings("ConstantConditions")
    @RequestMapping(path = "/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}/data", method = RequestMethod.GET)
    public ResponseEntity<FileSystemResource> getAttachmentData(@PathVariable String attachmentId,
            HttpServletResponse response) {

        Attachment attachment = attachmentService.getAttachment(attachmentId);
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + attachment.getTitle() + attachment.getExtension() + "\"");

        return new ResponseEntity<>(attachmentService.getAttachmentData(attachment), HttpStatus.OK);
    }

    @RequestMapping(path = "/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.PATCH)
    public ResponseEntity<Attachment> updateAttachment(@PathVariable String attachmentId,
            @RequestBody @Valid UpdateAttachmentRequestDto updateAttachment) throws IOException {
        return new ResponseEntity<>(attachmentService.updateAttachment(attachmentId, updateAttachment), HttpStatus.OK);
    }

    @RequestMapping(path = "/attachments/{attachmentId:^20\\d{2}-\\d{1,2}-\\d{1,2}-\\d+$}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAttachment(@PathVariable String attachmentId) throws IOException {
        attachmentService.deleteAttachment(attachmentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /* - USER ADMIN API - */
    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<UserAdminLinksDto>> getAllUsers(
            @RequestParam(name = "maxSize", defaultValue = "5000", required = false) long maxSize,
            @RequestParam(name = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(name = "sortType", defaultValue = "asc", required = false) String sortType) {

        Comparator<User> comparator = Utils.parseUserSortingType(sortBy, sortType);
        return new ResponseEntity<>(userService.getAllUsers(maxSize, comparator), HttpStatus.OK);
    }

    @RequestMapping(path = "/users/{userId}", method = RequestMethod.GET)
    public ResponseEntity<UserAdminLinksDto> getUser(@PathVariable String userId) {
        return new ResponseEntity<>(userService.getUser(userId), HttpStatus.OK);
    }

    @RequestMapping(path = "/users/{userId}/author", method = RequestMethod.GET)
    public ResponseEntity<UserItemsAuthorResponseDto> getUsersAuthorItems(@PathVariable String userId) {
        return new ResponseEntity<>(userService.getUserAuthorItems(userId), HttpStatus.OK);
    }

    @RequestMapping(path = "/users/{userId}", method = RequestMethod.PATCH)
    public ResponseEntity<UserAdminLinksDto> updateUser(@PathVariable String userId,
            @RequestBody @Valid UpdateUserRequestDto updateUser) {
        return new ResponseEntity<>(userService.updateUser(userId, updateUser), HttpStatus.OK);
    }

    @RequestMapping(path = "/users/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable String userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
