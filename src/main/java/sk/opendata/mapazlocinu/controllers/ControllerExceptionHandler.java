/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.controllers;

import org.cyberborean.rdfbeans.exceptions.RDFBeanException;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import sk.opendata.mapazlocinu.dto.ErrorMessageResponseDto;
import sk.opendata.mapazlocinu.exception.AttachmentException;
import sk.opendata.mapazlocinu.exception.IncorrectVisibilityChangeException;
import sk.opendata.mapazlocinu.exception.InvalidIncidentTypeException;
import sk.opendata.mapazlocinu.exception.InvalidVisibilityException;
import sk.opendata.mapazlocinu.exception.RDFBeanRuntimeException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("unused")
@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);
    private final HttpServletRequest httpServletRequest;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public ControllerExceptionHandler(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    @ExceptionHandler(value = {RDFBeanException.class, RepositoryException.class, RDF4JException.class,
            RDFBeanRuntimeException.class})
    protected ResponseEntity<ErrorMessageResponseDto> handleRdfDatabaseEx(Exception ex) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(handleException(ex, status), status);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class, UsernameNotFoundException.class})
    protected ResponseEntity<ErrorMessageResponseDto> handleNotFoundEx(Exception ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(handleException(ex, status), status);
    }

    @ExceptionHandler(value = {EntityExistsException.class, IncorrectVisibilityChangeException.class,
            AttachmentException.class, InvalidIncidentTypeException.class, InvalidVisibilityException.class})
    protected ResponseEntity<ErrorMessageResponseDto> handleExForBadRequest(Exception ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(handleException(ex, status), status);
    }

    private ErrorMessageResponseDto handleException(Exception ex, HttpStatus status) {
        log.warn("Error occurred while processing request (caught by ControllerExceptionHandler): " + ex.getMessage());

        final ErrorMessageResponseDto errorMessage = new ErrorMessageResponseDto();
        errorMessage.setError(status.getReasonPhrase());
        errorMessage.setException(ex.getClass().getCanonicalName());
        errorMessage.setMessage(ex.getMessage());
        errorMessage.setStatus(status.value());
        errorMessage.setTimestamp(System.currentTimeMillis());
        errorMessage.setPath(httpServletRequest.getServletPath());

        return errorMessage;
    }
}
