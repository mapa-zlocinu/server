/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.utils;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentAdminDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentPublicDto;
import sk.opendata.mapazlocinu.dto.comment.CommentAdminDto;
import sk.opendata.mapazlocinu.dto.comment.CommentPublicDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminExcerptDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicExcerptDto;
import sk.opendata.mapazlocinu.dto.user.UserAdminDto;
import sk.opendata.mapazlocinu.dto.user.UserAdminLinksDto;
import sk.opendata.mapazlocinu.dto.user.UserPublicDto;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.Incident;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.entity.User;

import java.util.List;

@Component
public class ObjectsMapper {
    private final MapperFactory mapperFactory;

    public ObjectsMapper() {
        this.mapperFactory = new DefaultMapperFactory.Builder().build();
        this.configure();
    }

    private void configure() {
        mapperFactory.classMap(IncidentExcerpt.class, IncidentPublicExcerptDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(Incident.class, IncidentPublicDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(User.class, UserPublicDto.class)
                .field("id", "email")
                .byDefault()
                .register();

        mapperFactory.classMap(Comment.class, CommentPublicDto.class)
                .byDefault()
                .register();

        mapperFactory.classMap(IncidentExcerpt.class, IncidentAdminExcerptDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(Incident.class, IncidentAdminDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(User.class, UserAdminDto.class)
                .field("id", "email")
                .byDefault()
                .register();

        mapperFactory.classMap(Comment.class, CommentAdminDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(Attachment.class, AttachmentAdminDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();

        mapperFactory.classMap(User.class, UserAdminLinksDto.class)
                .field("id", "email")
                .byDefault()
                .register();

        mapperFactory.classMap(Attachment.class, AttachmentPublicDto.class)
                .field("id", "itemId")
                .byDefault()
                .register();
    }

    public List<IncidentPublicExcerptDto> toPublicIncidentsListDto(List<IncidentExcerpt> incidents) {
        return mapperFactory.getMapperFacade().mapAsList(incidents, IncidentPublicExcerptDto.class);
    }

    public IncidentPublicDto toPublicIncidentDto(Incident incident) {
        return mapperFactory.getMapperFacade().map(incident, IncidentPublicDto.class);
    }

    public List<CommentPublicDto> toPublicCommentsListDto(List<Comment> comments) {
        return mapperFactory.getMapperFacade().mapAsList(comments, CommentPublicDto.class);
    }

    public List<IncidentAdminExcerptDto> toAdminIncidentsListDto(List<IncidentExcerpt> incidents) {
        return mapperFactory.getMapperFacade().mapAsList(incidents, IncidentAdminExcerptDto.class);
    }

    public IncidentAdminDto toAdminIncidentDto(Incident incident) {
        return mapperFactory.getMapperFacade().map(incident, IncidentAdminDto.class);
    }

    public List<CommentAdminDto> toAdminCommentsListDto(List<Comment> comments) {
        return mapperFactory.getMapperFacade().mapAsList(comments, CommentAdminDto.class);
    }

    public CommentAdminDto toAdminCommentDto(Comment comment) {
        return mapperFactory.getMapperFacade().map(comment, CommentAdminDto.class);
    }

    public List<AttachmentAdminDto> toAdminAttachmentsListDto(List<Attachment> attachments) {
        return mapperFactory.getMapperFacade().mapAsList(attachments, AttachmentAdminDto.class);
    }

    public AttachmentAdminDto toAdminAttachmentDto(Attachment attachment) {
        return mapperFactory.getMapperFacade().map(attachment, AttachmentAdminDto.class);
    }

    public List<UserAdminLinksDto> toAdminUsersLinksListDto(List<User> users) {
        return mapperFactory.getMapperFacade().mapAsList(users, UserAdminLinksDto.class);
    }

    public UserAdminLinksDto toAdminUserLinksDto(User user) {
        return mapperFactory.getMapperFacade().map(user, UserAdminLinksDto.class);
    }

    public List<AttachmentPublicDto> toPublicAttachmentsListDto(List<Attachment> attachments) {
        return mapperFactory.getMapperFacade().mapAsList(attachments, AttachmentPublicDto.class);
    }
}

// tutorial source: http://www.baeldung.com/orika-mapping
