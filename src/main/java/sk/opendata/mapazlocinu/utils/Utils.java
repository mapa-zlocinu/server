/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.utils;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.enums.sorting.AttachmentSortingType;
import sk.opendata.mapazlocinu.dto.enums.sorting.CommentSortingType;
import sk.opendata.mapazlocinu.dto.enums.sorting.IncidentSortingType;
import sk.opendata.mapazlocinu.dto.enums.sorting.UserSortingType;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.exception.InvalidIncidentTypeException;
import sk.opendata.mapazlocinu.exception.InvalidVisibilityException;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);
    private static final SimpleDateFormat defaultDatabaseFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    public static void copyNonNullProperties(Object from, Object to) {
        BeanUtils.copyProperties(from, to, getNullPropertyNames(from));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }

        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    @SuppressWarnings("unused")
    public static <T> void iterableToCollection(@NotNull Iterable<T> src, @NotNull Collection<T> dst) {
        for (T item : src) {
            dst.add(item);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> void iterableToCollection(@NotNull CloseableIteration src, @NotNull Collection<T> dst) {
        try {
            while (src.hasNext()) {
                dst.add((T) src.next());
            }
        } catch (Exception e) {
            log.error("Error occurred while copying CloseableIteration to Collection.", e);
        }
    }

    public static String getDateFormattedId() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DAY_OF_MONTH) + "-";
    }

    public static Visibility processVisibilityFromString(String value) throws InvalidVisibilityException {
        if (value == null || value.isEmpty()) {
            throw new InvalidVisibilityException(
                    "Visibility can only have these values: " + Arrays.toString(Visibility.values()));
        }

        Visibility visibilityEnum;
        try {
            visibilityEnum = Visibility.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidVisibilityException(
                    "Visibility can only have these values: " + Arrays.toString(Visibility.values()));
        }

        return visibilityEnum;
    }

    public static IncidentType processIncidentTypeFromString(String value)
            throws InvalidIncidentTypeException {
        if (value == null || value.isEmpty()) {
            throw new InvalidIncidentTypeException(
                    "Incident type can only have these values: " + Arrays.toString(IncidentType.values()));
        }

        IncidentType incidentType;
        try {
            incidentType = IncidentType.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidIncidentTypeException(
                    "Incident type can only have these values: " + Arrays.toString(IncidentType.values()));
        }

        return incidentType;
    }

    public static Comparator<IncidentExcerpt> parseIncidentSortingType(@NotNull String sortBy,
            @NotNull String sortType) {
        IncidentSortingType incidentSortingType;
        try {
            incidentSortingType = IncidentSortingType.valueOf(sortBy.toUpperCase());
        } catch (IllegalArgumentException e) {
            incidentSortingType = IncidentSortingType.DATE;
        }

        Comparator<IncidentExcerpt> comparator;
        switch (incidentSortingType) {
            case TITLE:
                comparator = Comparator.comparing(IncidentExcerpt::getTitle);
                break;
            case TYPE:
                comparator = Comparator.comparing(IncidentExcerpt::getType);
                break;
            case ID:
                comparator = Comparator.comparing(IncidentExcerpt::getId);
                break;
            default:
                comparator = Comparator.comparing(IncidentExcerpt::getIncidentDate);
                break;
        }

        if (sortType.equalsIgnoreCase("desc")) {
            comparator = comparator.reversed();
        }

        return comparator;
    }

    public static Comparator<Comment> parseCommentSortingType(@NotNull String sortBy, @NotNull String sortType) {
        CommentSortingType commentSortingType;
        try {
            commentSortingType = CommentSortingType.valueOf(sortBy.toUpperCase());
        } catch (IllegalArgumentException e) {
            commentSortingType = CommentSortingType.DATE;
        }

        Comparator<Comment> comparator;
        switch (commentSortingType) {
            case ID:
                comparator = Comparator.comparing(Comment::getId);
                break;
            case TYPE:
                comparator = Comparator.comparing(Comment::getType);
                break;
            default:
                comparator = Comparator.comparing(Comment::getCreatedDate);
                break;
        }

        if (sortType.equalsIgnoreCase("desc")) {
            comparator = comparator.reversed();
        }

        return comparator;
    }

    public static Comparator<Attachment> parseAttachmentSortingType(@NotNull String sortBy, @NotNull String sortType) {
        AttachmentSortingType attachmentSortingType;
        try {
            attachmentSortingType = AttachmentSortingType.valueOf(sortBy.toUpperCase());
        } catch (IllegalArgumentException e) {
            attachmentSortingType = AttachmentSortingType.DATE;
        }

        Comparator<Attachment> comparator;
        switch (attachmentSortingType) {
            case ID:
                comparator = Comparator.comparing(Attachment::getId);
                break;
            case TITLE:
                comparator = Comparator.comparing(Attachment::getTitle);
                break;
            case SIZE:
                comparator = Comparator.comparing(Attachment::getSize);
                break;
            case EXTENSION:
                comparator = Comparator.comparing(Attachment::getExtension);
                break;
            default:
                comparator = Comparator.comparing(Attachment::getCreatedDate);
                break;
        }

        if (sortType.equalsIgnoreCase("desc")) {
            comparator = comparator.reversed();
        }

        return comparator;
    }

    public static Comparator<User> parseUserSortingType(@NotNull String sortBy, @NotNull String sortType) {
        UserSortingType userSortingType;
        try {
            userSortingType = UserSortingType.valueOf(sortBy.toUpperCase());
        } catch (IllegalArgumentException e) {
            userSortingType = UserSortingType.ID;
        }

        Comparator<User> comparator;
        switch (userSortingType) {
            case NAME:
                comparator = Comparator.comparing(User::getName);
                break;
            case RATING:
                comparator = Comparator.comparing(User::getRating);
                break;
            default:
                comparator = Comparator.comparing(User::getId);
                break;
        }

        if (sortType.equalsIgnoreCase("desc")) {
            comparator = comparator.reversed();
        }

        return comparator;
    }

    public static int parseNumberFromString(String input) {
        if (StringUtils.isEmpty(input)) {
            return 0;
        }

        try {
            return Integer.valueOf(input);
        } catch (NumberFormatException e) {
            log.error("Error occurred while parsing number value from input '" + input + "'.", e);
        }

        return 0;
    }

    @SuppressWarnings("unused")
    public static long parseNumberFromStringToLong(String input) {
        if (StringUtils.isEmpty(input)) {
            return 0;
        }

        try {
            return Long.valueOf(input);
        } catch (NumberFormatException e) {
            log.error("Error occurred while parsing number value from input '" + input + "'.", e);
        }

        return 0;
    }

    public static double parseNumberFromStringToDouble(String input) {
        if (StringUtils.isEmpty(input)) {
            return 0;
        }

        try {
            return Double.valueOf(input);
        } catch (NumberFormatException e) {
            log.error("Error occurred while parsing number value from input '" + input + "'.", e);
        }

        return 0;
    }

    public static Date parseDateFromDB(String input) {
        try {
            return defaultDatabaseFormat.parse(input);
        } catch (ParseException e) {
            log.error("Error while parsing date value " + input + ".", e);
        }

        return new Date();
    }
}
