/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.comment;

import org.hibernate.validator.constraints.NotBlank;
import sk.opendata.mapazlocinu.config.ValidationConstants;
import sk.opendata.mapazlocinu.dto.enums.CommentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

public class UpdateCommentRequestDto {
    @NotBlank
    @Size(min = ValidationConstants.TEXT_MIN_LENGTH, max = ValidationConstants.TEXT_MAX_LENGTH)
    private String text;

    @NotNull
    private Boolean authorVisible;

    @NotNull
    @Past
    private Date createdDate;

    @NotNull
    private Visibility visibility;

    @NotNull
    private CommentType type;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(Boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public CommentType getType() {
        return type;
    }

    public void setType(CommentType type) {
        this.type = type;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }
}
