/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import sk.opendata.mapazlocinu.controllers.AdminController;
import sk.opendata.mapazlocinu.dto.enums.CommentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.user.UserAdminDto;

import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class CommentAdminDto extends ResourceSupport {
    @JsonProperty("id")
    private String itemId;
    private String text;
    private UserAdminDto author;
    private boolean authorVisible;
    private Date createdDate;
    private Visibility visibility;
    private CommentType type;

    public CommentAdminDto(String itemId,
            String text,
            UserAdminDto author,
            boolean authorVisible,
            Date createdDate, Visibility visibility, CommentType type) {
        this.itemId = itemId;
        this.text = text;
        this.author = author;
        this.authorVisible = authorVisible;
        this.createdDate = createdDate;
        this.visibility = visibility;
        this.type = type;

        add(linkTo(methodOn(AdminController.class).getComment(getItemId())).withSelfRel());
        add(linkTo(methodOn(AdminController.class).getIncidentForComment(getItemId())).withRel("comment-of-incident"));
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserAdminDto getAuthor() {
        return author;
    }

    public void setAuthor(UserAdminDto author) {
        this.author = author;
    }

    public boolean isAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public CommentType getType() {
        return type;
    }

    public void setType(CommentType type) {
        this.type = type;
    }
}
