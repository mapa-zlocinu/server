/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto;

import org.springframework.hateoas.ResourceSupport;
import sk.opendata.mapazlocinu.controllers.AdminController;

import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class DataSummaryResponseDto extends ResourceSupport {
    private int numberOfIncidents;
    private int numberOfComments;
    private int numberOfAttachments;
    private int numberOfUsers;

    private Map<String, Integer> incidentsByVisibility;
    private Map<String, Integer> incidentsByType;
    private Map<String, Integer> commentsByVisibility;

    public DataSummaryResponseDto() {
        add(linkTo(methodOn(AdminController.class).getDataSummary()).withSelfRel());

        add(linkTo(methodOn(AdminController.class).getAllIncidents(2500L, "date", "asc")).withRel("all-incidents"));
        add(linkTo(methodOn(AdminController.class).getAllPendingIncidents(2500L, "date", "asc"))
                .withRel("pending-incidents"));
        add(linkTo(methodOn(AdminController.class).getAllDisapprovedIncidents(2500L, "date", "asc"))
                .withRel("disapproved-incidents"));

        add(linkTo(methodOn(AdminController.class).getAllComments(2500L, "date", "asc")).withRel("all-comments"));
        add(linkTo(methodOn(AdminController.class).getAllPendingComments(2500L, "date", "asc"))
                .withRel("pending-comments"));
        add(linkTo(methodOn(AdminController.class).getAllDisapprovedComments(2500L, "date", "asc"))
                .withRel("disapproved-comments"));

        add(linkTo(methodOn(AdminController.class).getAllAttachments(2500L, "date", "asc")).withRel("all-attachments"));
        add(linkTo(methodOn(AdminController.class).getAllUsers(2500L, "id", "asc")).withRel("all-users"));
    }

    public int getNumberOfIncidents() {
        return numberOfIncidents;
    }

    public void setNumberOfIncidents(int numberOfIncidents) {
        this.numberOfIncidents = numberOfIncidents;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public int getNumberOfAttachments() {
        return numberOfAttachments;
    }

    public void setNumberOfAttachments(int numberOfAttachments) {
        this.numberOfAttachments = numberOfAttachments;
    }

    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    public Map<String, Integer> getIncidentsByVisibility() {
        return incidentsByVisibility;
    }

    public void setIncidentsByVisibility(Map<String, Integer> incidentsByVisibility) {
        this.incidentsByVisibility = incidentsByVisibility;
    }

    public Map<String, Integer> getIncidentsByType() {
        return incidentsByType;
    }

    public void setIncidentsByType(Map<String, Integer> incidentsByType) {
        this.incidentsByType = incidentsByType;
    }

    public Map<String, Integer> getCommentsByVisibility() {
        return commentsByVisibility;
    }

    public void setCommentsByVisibility(Map<String, Integer> commentsByVisibility) {
        this.commentsByVisibility = commentsByVisibility;
    }
}
