/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.incident;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import sk.opendata.mapazlocinu.controllers.IncidentController;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;

import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class IncidentPublicExcerptDto extends ResourceSupport {
    @JsonProperty("id")
    private String itemId;
    private String title;
    private Date incidentDate;
    private double lat;
    private double lon;
    private IncidentType type;

    public IncidentPublicExcerptDto(String itemId,
            String title,
            Date incidentDate,
            double lat,
            double lon,
            IncidentType type) {
        this.itemId = itemId;
        this.title = title;
        this.incidentDate = incidentDate;
        this.lat = lat;
        this.lon = lon;
        this.type = type;

        add(linkTo(methodOn(IncidentController.class).getIncidentDetail(getItemId())).withSelfRel());
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public IncidentType getType() {
        return type;
    }

    public void setType(IncidentType type) {
        this.type = type;
    }
}
