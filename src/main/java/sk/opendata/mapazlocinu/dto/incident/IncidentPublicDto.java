/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.incident;

import sk.opendata.mapazlocinu.controllers.IncidentController;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.user.UserPublicDto;

import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class IncidentPublicDto extends IncidentPublicExcerptDto {
    private String description;
    private Date createdDate;
    private UserPublicDto author;
    private boolean authorVisible;
    private String locationAddress;

    public IncidentPublicDto(String itemId,
            String title,
            Date incidentDate,
            double lat,
            double lon,
            IncidentType type,
            String description,
            Date createdDate,
            UserPublicDto author,
            boolean authorVisible,
            String locationAddress) {
        super(itemId, title, incidentDate, lat, lon, type);
        this.description = description;
        this.createdDate = createdDate;
        this.author = author;
        this.authorVisible = authorVisible;
        this.locationAddress = locationAddress;

        add(linkTo(methodOn(IncidentController.class).getIncidentComments(getItemId(), 2500L, "date", "asc"))
                .withRel("comments"));
        add(linkTo(methodOn(IncidentController.class).getIncidentAttachmentsList(getItemId(), 2500L, "date", "asc"))
                .withRel("attachments"));
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public UserPublicDto getAuthor() {
        return author;
    }

    public void setAuthor(UserPublicDto author) {
        this.author = author;
    }

    public boolean isAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }
}
