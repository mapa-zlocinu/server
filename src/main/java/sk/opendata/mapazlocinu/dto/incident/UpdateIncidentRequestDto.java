/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.incident;

import org.hibernate.validator.constraints.NotBlank;
import sk.opendata.mapazlocinu.config.ValidationConstants;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

public class UpdateIncidentRequestDto {
    @NotBlank
    @Size(min = ValidationConstants.TITLE_MIN_LENGTH, max = ValidationConstants.TITLE_MAX_LENGTH)
    private String title;

    @NotBlank
    @Size(min = ValidationConstants.TEXT_MIN_LENGTH, max = ValidationConstants.TEXT_MAX_LENGTH)
    private String description;

    @Past
    @NotNull
    private Date incidentDate;

    @Past
    @NotNull
    private Date createdDate;

    @NotNull
    private Boolean authorVisible;

    @NotNull
    private Double lat;

    @NotNull
    private Double lon;

    @NotBlank
    @Size(min = ValidationConstants.TITLE_MIN_LENGTH, max = ValidationConstants.TITLE_MAX_LENGTH)
    private String locationAddress;

    @NotNull
    private Visibility visibility;

    @NotNull
    private IncidentType type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(Boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public IncidentType getType() {
        return type;
    }

    public void setType(IncidentType type) {
        this.type = type;
    }
}
