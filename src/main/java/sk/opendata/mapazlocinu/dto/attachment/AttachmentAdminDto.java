/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto.attachment;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;
import sk.opendata.mapazlocinu.controllers.AdminController;

import java.util.Date;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class AttachmentAdminDto extends ResourceSupport {
    @JsonProperty("id")
    private String itemId;
    private String title;
    private String extension;
    private String contentType;
    private Long size;
    private Date createdDate;

    public AttachmentAdminDto(String itemId,
            String title,
            String extension,
            String contentType,
            Long size,
            Date createdDate) {
        this.itemId = itemId;
        this.title = title;
        this.extension = extension;
        this.contentType = contentType;
        this.size = size;
        this.createdDate = createdDate;

        add(linkTo(methodOn(AdminController.class).getAttachment(getItemId())).withSelfRel());
        add(linkTo(methodOn(AdminController.class).getIncidentForAttachment(getItemId()))
                .withRel("attachment-of-incident"));
        add(linkTo(methodOn(AdminController.class).getAttachmentData(getItemId(), null)).withRel("attachment-data"));
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
