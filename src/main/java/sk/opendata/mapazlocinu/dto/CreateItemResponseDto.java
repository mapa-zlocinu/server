/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.dto;

import sk.opendata.mapazlocinu.dto.enums.CreateResponseItemType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;

public class CreateItemResponseDto {
    private String itemId;
    private Visibility itemStatus;
    private CreateResponseItemType itemType;

    public CreateItemResponseDto() {
    }

    public CreateItemResponseDto(String itemId,
            Visibility itemStatus,
            CreateResponseItemType itemType) {
        this.itemId = itemId;
        this.itemStatus = itemStatus;
        this.itemType = itemType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Visibility getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Visibility itemStatus) {
        this.itemStatus = itemStatus;
    }

    public CreateResponseItemType getItemType() {
        return itemType;
    }

    public void setItemType(CreateResponseItemType itemType) {
        this.itemType = itemType;
    }
}
