/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.config;

import org.cyberborean.rdfbeans.RDFBeanManager;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.config.RepositoryConfigException;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RdfRepositoryConnector {
    private static final Logger log = LoggerFactory.getLogger(RdfRepositoryConnector.class);

    private final RemoteRepositoryManager repositoryManager;
    private final Repository repository;
    private final RDFBeanManager rdfBeanManager;
    private final String repositoryUrl;
    private final String repositoryId;

    @Autowired
    public RdfRepositoryConnector(@Value("${rdf4j.server.url}") String repositoryUrl,
            @Value("${rdf4j.server.repository-id}") String repositoryId)
            throws RepositoryException, RepositoryConfigException {
        // another possibility of init is usage of new HTTPRepository(repositoryUrl, repositoryId);
        log.info("Initializing RDF4J connector bean -> URL = " + repositoryUrl + ", ID = " + repositoryId);

        this.repositoryUrl = repositoryUrl;
        this.repositoryId = repositoryId;

        this.repositoryManager = new RemoteRepositoryManager(repositoryUrl);
        this.repositoryManager.initialize();

        this.repository = this.repositoryManager.getRepository(repositoryId);
        this.rdfBeanManager = new RDFBeanManager(this.repository.getConnection());

        log.info("Initialization of RDF4J connector finished.");
    }

    @SuppressWarnings("unused")
    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    @SuppressWarnings("unused")
    public String getRepositoryId() {
        return repositoryId;
    }

    @SuppressWarnings("unused")
    public RemoteRepositoryManager getRepositoryManager() {
        return repositoryManager;
    }

    @SuppressWarnings("unused")
    public Repository getRepository() {
        return repository;
    }

    @SuppressWarnings("unused")
    public RDFBeanManager getRDFBeanManager() {
        return rdfBeanManager;
    }
}
