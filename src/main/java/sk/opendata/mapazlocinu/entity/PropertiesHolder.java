/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.entity;

import org.cyberborean.rdfbeans.annotations.RDF;
import org.cyberborean.rdfbeans.annotations.RDFBean;
import org.cyberborean.rdfbeans.annotations.RDFNamespaces;
import org.cyberborean.rdfbeans.annotations.RDFSubject;

@RDFNamespaces({
        "mz = http://mapazlocinu.opendata.sk/"
})
@RDFBean("mz:PropertiesHolder")
public class PropertiesHolder {
    private String id;
    private long lastIncidentId;
    private long lastCommentId;
    private long lastAttachmentId;

    public PropertiesHolder() {
    }

    @RDFSubject(prefix = "http://mapazlocinu.opendata.sk/propertiesSet/")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @RDF("mz:lastIncidentId")
    public long getLastIncidentId() {
        return lastIncidentId;
    }

    public void setLastIncidentId(long lastIncidentId) {
        this.lastIncidentId = lastIncidentId;
    }

    @RDF("mz:lastCommentId")
    public long getLastCommentId() {
        return lastCommentId;
    }

    public void setLastCommentId(long lastCommentId) {
        this.lastCommentId = lastCommentId;
    }

    @RDF("mz:lastAttachmentId")
    public long getLastAttachmentId() {
        return lastAttachmentId;
    }

    public void setLastAttachmentId(long lastAttachmentId) {
        this.lastAttachmentId = lastAttachmentId;
    }
}
