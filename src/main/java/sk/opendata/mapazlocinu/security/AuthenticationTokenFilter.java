/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import sk.opendata.mapazlocinu.security.jwt.JsonWebTokenUtils;
import sk.opendata.mapazlocinu.security.model.AdminAuthenticationDetails;
import sk.opendata.mapazlocinu.security.services.impl.UserDetailsServiceImpl;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {
    private final JsonWebTokenUtils jsonWebTokenUtils;
    private final UserDetailsServiceImpl userDetailsService;

    public AuthenticationTokenFilter(JsonWebTokenUtils jsonWebTokenUtils,
            UserDetailsServiceImpl userDetailsService) {
        this.jsonWebTokenUtils = jsonWebTokenUtils;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        String authToken = extractAuthToken(httpRequest.getHeader("Authorization"));

        if (authToken == null) {
            authToken = httpRequest.getHeader("api_key");
        }

        final String username = jsonWebTokenUtils.getUsernameFromToken(authToken);

        if (username != null && isNotAuthenticated(SecurityContextHolder.getContext().getAuthentication())) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            if (jsonWebTokenUtils.validateToken(authToken, (AdminAuthenticationDetails) userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } else if (SecurityContextHolder.getContext().getAuthentication() == null) {
            SecurityContextHolder.getContext().setAuthentication(
                    new AnonymousAuthenticationToken("anonymousToken", "anonymous", anonymousAuthority()));
        }

        chain.doFilter(request, response);
    }

    private List<GrantedAuthority> anonymousAuthority() {
        final List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));
        return authorities;
    }

    private String extractAuthToken(String authHeader) {
        if (authHeader != null) {
            Pattern p = Pattern.compile("[Bb]earer (.*)");
            Matcher m = p.matcher(authHeader);

            if (m.find()) {
                return m.group(1);
            }
        }

        return null;
    }

    private boolean isNotAuthenticated(Authentication authentication) {
        return authentication == null || !(authentication.isAuthenticated()) ||
                authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));
    }
}

