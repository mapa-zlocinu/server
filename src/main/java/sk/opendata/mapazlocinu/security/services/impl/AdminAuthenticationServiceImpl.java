/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.security.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.security.LoginRequest;
import sk.opendata.mapazlocinu.dto.security.LoginResponse;
import sk.opendata.mapazlocinu.entity.Admin;
import sk.opendata.mapazlocinu.repository.AdminRepository;
import sk.opendata.mapazlocinu.security.jwt.JsonWebTokenUtils;
import sk.opendata.mapazlocinu.security.model.AdminAuthenticationDetails;
import sk.opendata.mapazlocinu.security.services.AdminAuthenticationService;

import javax.annotation.PostConstruct;

@Service
public class AdminAuthenticationServiceImpl implements AdminAuthenticationService {

    private final AdminRepository adminRepository;
    private final AuthenticationManager authenticationManager;
    private final JsonWebTokenUtils tokenUtils;
    private final PasswordEncoder passwordEncoder;

    private boolean initAdmin = false;
    private String adminInitUsername;
    private String adminInitPassword;

    @Autowired
    public AdminAuthenticationServiceImpl(AdminRepository adminRepository,
            AuthenticationManager authenticationManager,
            JsonWebTokenUtils tokenUtils, PasswordEncoder passwordEncoder) {
        this.adminRepository = adminRepository;
        this.authenticationManager = authenticationManager;
        this.tokenUtils = tokenUtils;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void initAdminCredentials() {
        if (initAdmin) {
            Admin admin = adminRepository.findById(adminInitUsername);

            if (admin == null) {
                admin = new Admin();
                admin.setUsername(adminInitUsername);
                admin.setPassword(passwordEncoder.encode(adminInitPassword));

                adminRepository.save(admin);
            }
        }
    }

    @Override
    public LoginResponse authenticateAdmin(LoginRequest request) throws AuthenticationException {
        Admin admin = adminRepository.findById(request.getUsername());

        Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return new LoginResponse(tokenUtils.generateToken(new AdminAuthenticationDetails(admin)));
    }

    @Value("${app.security.admin.init}")
    public void setInitAdmin(String initAdmin) {
        this.initAdmin = Boolean.valueOf(initAdmin);
    }

    @Value("${app.security.admin.username}")
    public void setAdminInitUsername(String adminInitUsername) {
        this.adminInitUsername = adminInitUsername;
    }

    @Value("${app.security.admin.password}")
    public void setAdminInitPassword(String adminInitPassword) {
        this.adminInitPassword = adminInitPassword;
    }
}
