/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services;

import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.comment.CommentAdminDto;
import sk.opendata.mapazlocinu.dto.comment.CommentPublicDto;
import sk.opendata.mapazlocinu.dto.comment.CreateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.comment.UpdateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.entity.Comment;

import java.util.Comparator;
import java.util.List;

public interface CommentService {
    /* Methods for PUBLIC API */
    CreateItemResponseDto createComment(String incidentId, CreateCommentRequestDto comment);

    List<CommentPublicDto> getIncidentApprovedComments(String incidentId, long maxSize, Comparator<Comment> comparator);

    /* Methods for ADMIN API */
    List<CommentAdminDto> getCommentsByVisibility(Visibility visibility, long maxSize, Comparator<Comment> comparator);

    IncidentAdminDto getIncidentForComment(String commentId);

    CommentAdminDto updateComment(String id, UpdateCommentRequestDto updateComment);

    void deleteComment(String id);

    CommentAdminDto updateCommentVisibility(String id, Visibility visibility);

    CommentAdminDto getComment(String id);

    List<CommentAdminDto> getAllComments(long maxSize, Comparator<Comment> comparator);

    List<CommentAdminDto> getIncidentComments(String incidentId, long maxSize, Comparator<Comment> comparator);
}
