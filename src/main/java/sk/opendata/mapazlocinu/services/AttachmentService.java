/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services;

import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;
import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentAdminDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentPublicDto;
import sk.opendata.mapazlocinu.dto.attachment.UpdateAttachmentRequestDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.entity.Attachment;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public interface AttachmentService {
    /* Methods for PUBLIC API */
    CreateItemResponseDto createAttachment(String incidentId, MultipartFile multipartFile) throws IOException;

    List<AttachmentPublicDto> getApprovedIncidentAttachments(String incidentId,
            long maxSize,
            Comparator<Attachment> comparator);

    Attachment getApprovedIncidentAttachment(String incidentId, String attachmentId);

    /* Methods for ADMIN API */
    IncidentAdminDto getIncidentForAttachment(String id);

    FileSystemResource getAttachmentData(Attachment attachment);

    Attachment updateAttachment(String id, UpdateAttachmentRequestDto updateAttachment) throws IOException;

    void deleteAttachment(String id) throws IOException;

    Attachment getAttachment(String id);

    AttachmentAdminDto getAttachmentWithLinks(String id);

    List<AttachmentAdminDto> getIncidentAttachments(String incidentId, long maxSize, Comparator<Attachment> comparator);

    List<AttachmentAdminDto> getAllAttachments(long maxSize, Comparator<Attachment> comparator);
}
