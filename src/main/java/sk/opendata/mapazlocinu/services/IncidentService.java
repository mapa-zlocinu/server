/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services;

import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.*;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;

import java.util.Comparator;
import java.util.List;

public interface IncidentService {
    /* Methods for PUBLIC API */
    CreateItemResponseDto createIncident(CreateIncidentRequestDto incident);

    List<IncidentPublicExcerptDto> getAllPublicIncidents(long maxSize, Comparator<IncidentExcerpt> comparator);

    IncidentPublicDto getPublicIncidentDetail(String id);

    List<IncidentPublicExcerptDto> getAllPublicIncidentsInBoundary(double upperLeftLat,
            double upperLeftLon,
            double bottomRightLat,
            double bottomRightLon,
            long maxSize,
            Comparator<IncidentExcerpt> comparator);

    List<IncidentPublicExcerptDto> getAllPublicIncidentsByType(IncidentType type,
            long maxSize,
            Comparator<IncidentExcerpt> comparator);

    /* Methods for ADMIN API */
    IncidentAdminDto getIncidentDetail(String id);

    List<IncidentAdminExcerptDto> getAllIncidentsExcerptByVisibility(Visibility visibility,
            long maxSize,
            Comparator<IncidentExcerpt> comparator);

    IncidentAdminDto updateIncident(String id, UpdateIncidentRequestDto incidentUpdate);

    void deleteIncident(String id);

    IncidentAdminDto updateIncidentVisibility(String id, Visibility visibility);

    List<IncidentAdminExcerptDto> getAllIncidentsExcerpt(long maxSize, Comparator<IncidentExcerpt> comparator);
}
