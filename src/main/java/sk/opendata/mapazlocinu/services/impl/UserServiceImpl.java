/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.user.UpdateUserRequestDto;
import sk.opendata.mapazlocinu.dto.user.UserAdminLinksDto;
import sk.opendata.mapazlocinu.dto.user.UserItemsAuthorResponseDto;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.repository.UserRepository;
import sk.opendata.mapazlocinu.services.UserService;
import sk.opendata.mapazlocinu.utils.ObjectsMapper;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ObjectsMapper objectsMapper;
    private int initRating;
    private int incrementRatingValue;
    private int decrementRatingValue;
    private int approvalRatingLimit;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ObjectsMapper objectsMapper) {
        this.userRepository = userRepository;
        this.objectsMapper = objectsMapper;
    }

    @Override
    public Visibility getRatingBasedVisibilityForUser(int rating) {
        return getVisibilityForUserRating(rating);
    }

    @Override
    public User getUserOrCreate(String id, String name) {
        User user = userRepository.findById(id);
        return user != null ? user : createUser(id, name);
    }

    @Override
    public User createUser(String id, String name) {
        if (userRepository.findById(id) != null) {
            throw new EntityExistsException(String.format("User with email (=id) %s already exists", id));
        }

        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setRating(initRating);
        userRepository.save(user);

        return user;
    }

    @Override
    public void updateScoreForUser(User user, Visibility input, Visibility output) {
        if (input.equals(output)) {
            return;
        }

        if (input.equals(Visibility.PENDING) && output.equals(Visibility.APPROVED)) {
            incrementUserRating(user);
        } else if (input.equals(Visibility.PENDING) && output.equals(Visibility.DISAPPROVED)) {
            decrementUserRating(user);
        } else if (input.equals(Visibility.APPROVED) && output.equals(Visibility.DISAPPROVED)) {
            decrementUserRating(user);
        } else if (input.equals(Visibility.DISAPPROVED) && output.equals(Visibility.APPROVED)) {
            incrementUserRating(user);
        }
    }

    @Override
    public void updateScoreForUser(User user, String input, Visibility output) {
        updateScoreForUser(user, Visibility.valueOf(input), output);
    }

    @Override
    public List<UserAdminLinksDto> getAllUsers(long maxSize, Comparator<User> comparator) {
        return objectsMapper.toAdminUsersLinksListDto(
                userRepository.findAll().stream()
                        .limit(maxSize)
                        .sorted(comparator)
                        .collect(Collectors.toList()));
    }

    @Override
    public UserAdminLinksDto updateUser(String id, UpdateUserRequestDto userUpdate) {
        User user = getNotNullUserOrElseThrowNotFound(id);
        Utils.copyNonNullProperties(userUpdate, user);
        userRepository.update(user);

        return objectsMapper.toAdminUserLinksDto(user);
    }

    @Override
    public void deleteUser(String id) {
        User user = getNotNullUserOrElseThrowNotFound(id);
        userRepository.delete(user.getId());
    }

    @Override
    public UserAdminLinksDto getUser(String id) {
        return objectsMapper.toAdminUserLinksDto(getNotNullUserOrElseThrowNotFound(id));
    }

    @Override
    public UserItemsAuthorResponseDto getUserAuthorItems(String id) {
        UserItemsAuthorResponseDto response = new UserItemsAuthorResponseDto();
        response.setAuthorOfIncidents(userRepository.getUsersIncidentsIds(id));
        response.setAuthorOfComments(userRepository.getUsersCommentsIds(id));

        return response;
    }

    private void incrementUserRating(User user) {
        if (user.getRating() < approvalRatingLimit) {
            user.setRating(user.getRating() + incrementRatingValue);
        }
        userRepository.update(user);
    }

    private void decrementUserRating(User user) {
        user.setRating(user.getRating() - decrementRatingValue);
        userRepository.update(user);
    }

    private User getNotNullUserOrElseThrowNotFound(String userId) {
        User user = userRepository.findById(userId);

        if (user == null) {
            throw new EntityNotFoundException(String.format("User with email (=id) %s was not found!", userId));
        }

        return user;
    }

    private Visibility getVisibilityForUserRating(int rating) {
        return rating > approvalRatingLimit ? Visibility.APPROVED : Visibility.PENDING;
    }

    @Value("${users.rating.init}")
    public void setInitRating(String initRating) {
        this.initRating = Integer.parseInt(initRating);
    }

    @Value("${users.rating.increment}")
    public void setIncrementRatingValue(String incrementRatingValue) {
        this.incrementRatingValue = Integer.parseInt(incrementRatingValue);
    }

    @Value("${users.rating.decrement}")
    public void setDecrementRatingValue(String decrementRatingValue) {
        this.decrementRatingValue = Integer.parseInt(decrementRatingValue);
    }

    @Value("${users.rating.approval-limit}")
    public void setApprovalRatingLimit(String approvalRatingLimit) {
        this.approvalRatingLimit = Integer.parseInt(approvalRatingLimit);
    }
}
