/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.eclipse.rdf4j.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.comment.CommentAdminDto;
import sk.opendata.mapazlocinu.dto.comment.CommentPublicDto;
import sk.opendata.mapazlocinu.dto.comment.CreateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.comment.UpdateCommentRequestDto;
import sk.opendata.mapazlocinu.dto.enums.CreateResponseItemType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.Incident;
import sk.opendata.mapazlocinu.entity.IncidentFull;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.repository.CommentsRepository;
import sk.opendata.mapazlocinu.repository.IncidentFullRepository;
import sk.opendata.mapazlocinu.repository.IncidentRepository;
import sk.opendata.mapazlocinu.services.CommentService;
import sk.opendata.mapazlocinu.services.PropertiesService;
import sk.opendata.mapazlocinu.services.UserService;
import sk.opendata.mapazlocinu.utils.ObjectsMapper;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    private final UserService userService;
    private final CommentsRepository commentsRepository;
    private final IncidentFullRepository incidentFullRepository;
    private final IncidentRepository incidentRepository;
    private final PropertiesService propertiesService;
    private final ObjectsMapper objectsMapper;

    @Autowired
    public CommentServiceImpl(UserService userService,
            CommentsRepository commentsRepository,
            IncidentFullRepository incidentFullRepository,
            IncidentRepository incidentRepository,
            PropertiesService propertiesService,
            ObjectsMapper objectsMapper) {
        this.userService = userService;
        this.commentsRepository = commentsRepository;
        this.incidentFullRepository = incidentFullRepository;
        this.incidentRepository = incidentRepository;
        this.propertiesService = propertiesService;
        this.objectsMapper = objectsMapper;
    }

    @Override
    public CreateItemResponseDto createComment(String incidentId, CreateCommentRequestDto comment) {

        IncidentFull incident = getNotNullApprovedIncidentOrThrowEx(incidentId);
        User author = userService.getUserOrCreate(comment.getAuthor().getEmail(), comment.getAuthor().getName());
        Visibility visibility = userService.getRatingBasedVisibilityForUser(author.getRating());

        Comment commentEntity = new Comment();
        Utils.copyNonNullProperties(comment, commentEntity);

        commentEntity.setId(Utils.getDateFormattedId() + propertiesService.getIdForCommentAndIncrement());
        commentEntity.setAuthor(author);
        commentEntity.setCreatedDate(new Date());
        commentEntity.setVisibility(visibility.getValue());
        commentEntity.setType(comment.getType().getValue());

        incident.setComments(addCommentToNotNullList(incident, commentEntity));
        incidentFullRepository.update(incident);

        return new CreateItemResponseDto(commentEntity.getId(), visibility, CreateResponseItemType.COMMENT);
    }

    @Override
    public List<CommentPublicDto> getIncidentApprovedComments(String incidentId,
            long maxSize,
            Comparator<Comment> comparator) {

        List<Comment> comments = getNotNullApprovedIncidentOrThrowEx(incidentId).getComments();
        if (comments == null) {
            return new ArrayList<>();
        }

        comments = comments.stream()
                .filter(item -> item.getVisibility().equals(Visibility.APPROVED.getValue()))
                .limit(maxSize)
                .sorted(comparator)
                .collect(Collectors.toList());
        comments.forEach(item -> {
            if (!item.isAuthorVisible())
                item.setAuthor(null);
        });

        return objectsMapper.toPublicCommentsListDto(comments);
    }

    @Override
    public List<CommentAdminDto> getCommentsByVisibility(Visibility visibility,
            long maxSize,
            Comparator<Comment> comparator) {

        return objectsMapper.toAdminCommentsListDto(
                commentsRepository.getItemsByVisibility(visibility, maxSize).stream()
                        .sorted(comparator)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public IncidentAdminDto getIncidentForComment(String commentId) {
        getNotNullCommentOrElseThrowEx(commentId);  // check if comment exists
        Resource incidentResource = commentsRepository.getIncidentIdForComment(commentId);

        if (incidentResource != null) {
            Incident incident = incidentRepository.findById(incidentResource);
            return incident != null ? objectsMapper.toAdminIncidentDto(incident) : null;
        }

        return null;
    }

    @Override
    public CommentAdminDto updateComment(String id, UpdateCommentRequestDto updateComment) {
        Comment comment = getNotNullCommentOrElseThrowEx(id);

        propertiesService.checkVisibilityChange(comment.getVisibility(), updateComment.getVisibility());
        userService.updateScoreForUser(comment.getAuthor(), comment.getVisibility(), updateComment.getVisibility());

        Utils.copyNonNullProperties(updateComment, comment);
        comment.setVisibility(updateComment.getVisibility().getValue());
        comment.setType(updateComment.getType().getValue());

        commentsRepository.update(comment);

        return objectsMapper.toAdminCommentDto(comment);
    }

    @Override
    public void deleteComment(String id) {
        Comment comment = getNotNullCommentOrElseThrowEx(id);
        Resource incidentResource = commentsRepository.getIncidentIdForComment(id);
        if (incidentResource == null) {
            throw new EntityNotFoundException(
                    String.format("Corresponding incident to comment with ID %s was not found.", id));
        }
        IncidentFull incident = getNotNullIncidentOrThrowEx(incidentResource);

        List<Comment> comments = incident.getComments();
        if (comments == null) {
            return;
        }

        comments.remove(comment);
        incident.setComments(comments);

        incidentFullRepository.update(incident);
        commentsRepository.delete(comment.getId());
    }

    @Override
    public CommentAdminDto updateCommentVisibility(String id, Visibility visibility) {
        Comment comment = getNotNullCommentOrElseThrowEx(id);

        propertiesService.checkVisibilityChange(comment.getVisibility(), visibility);
        userService.updateScoreForUser(comment.getAuthor(), comment.getVisibility(), visibility);

        comment.setVisibility(visibility.getValue());
        commentsRepository.update(comment);

        return objectsMapper.toAdminCommentDto(comment);
    }

    @Override
    public CommentAdminDto getComment(String id) {
        return objectsMapper.toAdminCommentDto(getNotNullCommentOrElseThrowEx(id));
    }

    @Override
    public List<CommentAdminDto> getAllComments(long maxSize, Comparator<Comment> comparator) {
        List<Comment> allComments = new ArrayList<>();

        for (Visibility visibility : Visibility.values()) {
            allComments.addAll(commentsRepository.getItemsByVisibility(visibility, maxSize));
        }

        return objectsMapper.toAdminCommentsListDto(
                allComments.stream()
                        .sorted(comparator)
                        .limit(maxSize)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public List<CommentAdminDto> getIncidentComments(String incidentId, long maxSize, Comparator<Comment> comparator) {
        List<Comment> comments = getNotNullIncidentOrThrowEx(incidentId).getComments();
        if (comments == null) {
            return new ArrayList<>();
        }

        return objectsMapper.toAdminCommentsListDto(
                comments.stream()
                        .limit(maxSize)
                        .sorted(comparator)
                        .collect(Collectors.toList()));
    }

    private IncidentFull getNotNullApprovedIncidentOrThrowEx(String incidentId) {
        IncidentFull incident = incidentFullRepository.findById(incidentId);
        if (incident == null || !incident.getVisibility().equals(Visibility.APPROVED.getValue())) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", incidentId));
        }

        return incident;
    }

    private IncidentFull getNotNullIncidentOrThrowEx(String incidentId) {
        IncidentFull incident = incidentFullRepository.findById(incidentId);
        if (incident == null) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", incidentId));
        }

        return incident;
    }

    private IncidentFull getNotNullIncidentOrThrowEx(Resource incidentResource) {
        IncidentFull incident = incidentFullRepository.findById(incidentResource);
        if (incident == null) {
            throw new EntityNotFoundException(
                    String.format("Incident with ID %s was not found.", incidentResource.stringValue()));
        }

        return incident;
    }

    private List<Comment> addCommentToNotNullList(IncidentFull incident, Comment comment) {
        List<Comment> commentList = incident.getComments() != null ? incident.getComments() : new ArrayList<>();
        commentList.add(comment);

        return commentList;
    }

    private Comment getNotNullCommentOrElseThrowEx(String id) {
        Comment comment = commentsRepository.findById(id);
        if (comment == null) {
            throw new EntityNotFoundException(String.format("Comment with ID %s was not found.", id));
        }

        return comment;
    }
}
