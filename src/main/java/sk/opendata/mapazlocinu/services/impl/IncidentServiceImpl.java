/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.enums.CreateResponseItemType;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.CreateIncidentRequestDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminExcerptDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicDto;
import sk.opendata.mapazlocinu.dto.incident.IncidentPublicExcerptDto;
import sk.opendata.mapazlocinu.dto.incident.UpdateIncidentRequestDto;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Comment;
import sk.opendata.mapazlocinu.entity.Incident;
import sk.opendata.mapazlocinu.entity.IncidentExcerpt;
import sk.opendata.mapazlocinu.entity.IncidentFull;
import sk.opendata.mapazlocinu.entity.User;
import sk.opendata.mapazlocinu.repository.AttachmentRepository;
import sk.opendata.mapazlocinu.repository.CommentsRepository;
import sk.opendata.mapazlocinu.repository.IncidentExcerptRepository;
import sk.opendata.mapazlocinu.repository.IncidentFullRepository;
import sk.opendata.mapazlocinu.repository.IncidentRepository;
import sk.opendata.mapazlocinu.services.IncidentService;
import sk.opendata.mapazlocinu.services.PropertiesService;
import sk.opendata.mapazlocinu.services.UserService;
import sk.opendata.mapazlocinu.utils.ObjectsMapper;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IncidentServiceImpl implements IncidentService {
    private final IncidentRepository incidentRepository;
    private final IncidentExcerptRepository incidentExcerptRepository;
    private final IncidentFullRepository incidentFullRepository;
    private final CommentsRepository commentsRepository;
    private final AttachmentRepository attachmentRepository;
    private final UserService userService;
    private final PropertiesService propertiesService;
    private final ObjectsMapper objectsMapper;

    @Autowired
    public IncidentServiceImpl(IncidentRepository incidentRepository,
            IncidentExcerptRepository incidentExcerptRepository,
            IncidentFullRepository incidentFullRepository,
            CommentsRepository commentsRepository,
            AttachmentRepository attachmentRepository, UserService userService,
            PropertiesService propertiesService, ObjectsMapper objectsMapper) {
        this.incidentRepository = incidentRepository;
        this.incidentExcerptRepository = incidentExcerptRepository;
        this.incidentFullRepository = incidentFullRepository;
        this.commentsRepository = commentsRepository;
        this.attachmentRepository = attachmentRepository;
        this.userService = userService;
        this.propertiesService = propertiesService;
        this.objectsMapper = objectsMapper;
    }

    @Override
    public CreateItemResponseDto createIncident(CreateIncidentRequestDto incident) {
        User author = userService.getUserOrCreate(incident.getAuthor().getEmail(), incident.getAuthor().getName());
        Visibility visibility = userService.getRatingBasedVisibilityForUser(author.getRating());

        Incident incidentEntity = new Incident();
        Utils.copyNonNullProperties(incident, incidentEntity);

        incidentEntity.setId(Utils.getDateFormattedId() + propertiesService.getIdForIncidentAndIncrement());
        incidentEntity.setCreatedDate(new Date());
        incidentEntity.setType(incident.getType().getValue());
        incidentEntity.setAuthor(author);
        incidentEntity.setVisibility(visibility.getValue());

        incidentRepository.save(incidentEntity);
        return new CreateItemResponseDto(incidentEntity.getId(), visibility, CreateResponseItemType.INCIDENT);
    }

    @Override
    public List<IncidentPublicExcerptDto> getAllPublicIncidents(long maxSize, Comparator<IncidentExcerpt> comparator) {
        List<IncidentExcerpt> incidents = incidentExcerptRepository.getItemsByVisibility(Visibility.APPROVED, maxSize)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        return objectsMapper.toPublicIncidentsListDto(incidents);
    }

    @Override
    public IncidentPublicDto getPublicIncidentDetail(String id) {
        Incident incident = incidentRepository.findById(id);

        if (incident == null || !incident.getVisibility().equals(Visibility.APPROVED.getValue())) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", id));
        }

        if (!incident.isAuthorVisible()) {
            incident.setAuthor(null);
        }

        return objectsMapper.toPublicIncidentDto(incident);
    }

    @Override
    public List<IncidentPublicExcerptDto> getAllPublicIncidentsInBoundary(double upperLeftLat,
            double upperLeftLon,
            double bottomRightLat,
            double bottomRightLon,
            long maxSize,
            Comparator<IncidentExcerpt> comparator) {

        List<IncidentExcerpt> incidents = incidentExcerptRepository.getItemsByVisibility(Visibility.APPROVED, maxSize)
                .stream()
                .filter(item -> upperLeftLat >= item.getLat() && item.getLat() >= bottomRightLat &&
                        upperLeftLon <= item.getLon() && item.getLon() <= bottomRightLon)
                .sorted(comparator)
                .collect(Collectors.toList());

        return objectsMapper.toPublicIncidentsListDto(incidents);
    }

    @Override
    public List<IncidentPublicExcerptDto> getAllPublicIncidentsByType(IncidentType type,
            long maxSize,
            Comparator<IncidentExcerpt> comparator) {

        List<IncidentExcerpt> incidents = incidentExcerptRepository.getItemsByVisibility(Visibility.APPROVED, maxSize)
                .stream()
                .filter(item -> item.getType().equals(type.getValue()))
                .sorted(comparator)
                .collect(Collectors.toList());

        return objectsMapper.toPublicIncidentsListDto(incidents);
    }

    @Override
    public IncidentAdminDto getIncidentDetail(String id) {
        return objectsMapper.toAdminIncidentDto(getNotNullIncidentOrThrowEx(id));
    }

    @Override
    public List<IncidentAdminExcerptDto> getAllIncidentsExcerptByVisibility(Visibility visibility, long maxSize,
            Comparator<IncidentExcerpt> comparator) {

        List<IncidentExcerpt> incidents = incidentExcerptRepository.getItemsByVisibility(visibility, maxSize)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        return objectsMapper.toAdminIncidentsListDto(incidents);
    }

    @Override
    public IncidentAdminDto updateIncident(String id, UpdateIncidentRequestDto incidentUpdate) {
        IncidentFull incident = getNotNullFullIncidentOrThrowEx(id);

        propertiesService.checkVisibilityChange(incident.getVisibility(), incidentUpdate.getVisibility());
        userService.updateScoreForUser(incident.getAuthor(), incident.getVisibility(), incidentUpdate.getVisibility());

        Utils.copyNonNullProperties(incidentUpdate, incident);
        incident.setVisibility(incidentUpdate.getVisibility().getValue());
        incident.setType(incidentUpdate.getType().getValue());

        incidentRepository.update(incident);

        return objectsMapper.toAdminIncidentDto(incident);
    }

    @Override
    public void deleteIncident(String id) {
        IncidentFull incidentFull = getNotNullFullIncidentOrThrowEx(id);

        if (incidentFull.getComments() != null) {
            for (Comment comment : incidentFull.getComments()) {
                commentsRepository.delete(comment.getId());
            }
        }

        if (incidentFull.getAttachments() != null) {
            for (Attachment attachment : incidentFull.getAttachments()) {
                attachmentRepository.delete(attachment.getId());
            }
        }

        incidentFullRepository.delete(id);
    }

    @Override
    public IncidentAdminDto updateIncidentVisibility(String id, Visibility visibility) {
        IncidentFull incident = getNotNullFullIncidentOrThrowEx(id);

        propertiesService.checkVisibilityChange(incident.getVisibility(), visibility);
        userService.updateScoreForUser(incident.getAuthor(), incident.getVisibility(), visibility);

        incident.setVisibility(visibility.getValue());
        incidentRepository.update(incident);

        return objectsMapper.toAdminIncidentDto(incident);
    }

    @Override
    public List<IncidentAdminExcerptDto> getAllIncidentsExcerpt(long maxSize, Comparator<IncidentExcerpt> comparator) {
        List<IncidentExcerpt> allIncidents = new ArrayList<>();

        for (Visibility visibility : Visibility.values()) {
            allIncidents.addAll(incidentExcerptRepository.getItemsByVisibility(visibility, maxSize));
        }

        return objectsMapper.toAdminIncidentsListDto(
                allIncidents.stream()
                        .sorted(comparator)
                        .limit(maxSize)
                        .collect(Collectors.toList())
        );
    }

    private Incident getNotNullIncidentOrThrowEx(String id) {
        Incident incident = incidentRepository.findById(id);
        if (incident == null) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", id));
        }

        return incident;
    }

    private IncidentFull getNotNullFullIncidentOrThrowEx(String id) {
        IncidentFull incident = incidentFullRepository.findById(id);
        if (incident == null) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", id));
        }

        return incident;
    }
}
