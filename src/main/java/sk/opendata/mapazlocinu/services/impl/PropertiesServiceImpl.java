/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.entity.PropertiesHolder;
import sk.opendata.mapazlocinu.exception.IncorrectVisibilityChangeException;
import sk.opendata.mapazlocinu.repository.PropertiesHolderRepository;
import sk.opendata.mapazlocinu.services.PropertiesService;

@Service
public class PropertiesServiceImpl implements PropertiesService {
    private static final String PROPERTIES_ID = "1";
    private static final long INIT_PROPERTY_ID = 0L;

    private final PropertiesHolderRepository propertiesHolderRepository;

    @Autowired
    public PropertiesServiceImpl(PropertiesHolderRepository propertiesHolderRepository) {
        this.propertiesHolderRepository = propertiesHolderRepository;
    }

    @Override
    public long getIdForIncidentAndIncrement() {
        PropertiesHolder properties = getNotNullPropertySet();
        properties.setLastIncidentId(properties.getLastIncidentId() + 1);
        propertiesHolderRepository.update(properties);

        return properties.getLastIncidentId();
    }

    @Override
    public long getIdForCommentAndIncrement() {
        PropertiesHolder properties = getNotNullPropertySet();
        properties.setLastCommentId(properties.getLastCommentId() + 1);
        propertiesHolderRepository.update(properties);

        return properties.getLastCommentId();
    }

    @Override
    public long getIdForAttachmentAndIncrement() {
        PropertiesHolder properties = getNotNullPropertySet();
        properties.setLastAttachmentId(properties.getLastAttachmentId() + 1);
        propertiesHolderRepository.update(properties);

        return properties.getLastAttachmentId();
    }

    @Override
    public void checkVisibilityChange(Visibility input, Visibility output) throws IncorrectVisibilityChangeException {
        // PENDING -> PENDING or APPROVED or DISAPPROVED
        // APPROVED -> APPROVED or DISAPPROVED
        // DISAPPROVED -> DISAPPROVED or APPROVED

        if (input.equals(Visibility.APPROVED) && output.equals(Visibility.PENDING)) {
            throw new IncorrectVisibilityChangeException("Item can not be updated from APPROVED to PENDING state.");
        } else if (input.equals(Visibility.DISAPPROVED) && output.equals(Visibility.PENDING)) {
            throw new IncorrectVisibilityChangeException("Item can not be updated from DISAPPROVED to PENDING state.");
        }
    }

    @Override
    public void checkVisibilityChange(String input, Visibility output) throws IncorrectVisibilityChangeException {
        checkVisibilityChange(Visibility.valueOf(input), output);
    }

    private PropertiesHolder getNotNullPropertySet() {
        PropertiesHolder loadedProperties = propertiesHolderRepository.findById(PROPERTIES_ID);

        if (loadedProperties == null) {
            loadedProperties = new PropertiesHolder();
            loadedProperties.setId(PROPERTIES_ID);
            loadedProperties.setLastIncidentId(INIT_PROPERTY_ID);
            loadedProperties.setLastCommentId(INIT_PROPERTY_ID);
            loadedProperties.setLastAttachmentId(INIT_PROPERTY_ID);

            propertiesHolderRepository.save(loadedProperties);
        }

        return loadedProperties;
    }
}
