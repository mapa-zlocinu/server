/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.opendata.mapazlocinu.dto.DataSummaryResponseDto;
import sk.opendata.mapazlocinu.dto.enums.IncidentType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.repository.AttachmentRepository;
import sk.opendata.mapazlocinu.repository.CommentsRepository;
import sk.opendata.mapazlocinu.repository.IncidentExcerptRepository;
import sk.opendata.mapazlocinu.repository.UserRepository;
import sk.opendata.mapazlocinu.services.AdminService;

import java.util.HashMap;
import java.util.Map;

@Service
public class AdminServiceImpl implements AdminService {
    private final IncidentExcerptRepository incidentExcerptRepository;
    private final CommentsRepository commentsRepository;
    private final AttachmentRepository attachmentRepository;
    private final UserRepository userRepository;

    @Autowired
    public AdminServiceImpl(IncidentExcerptRepository incidentExcerptRepository,
            CommentsRepository commentsRepository,
            AttachmentRepository attachmentRepository,
            UserRepository userRepository) {
        this.incidentExcerptRepository = incidentExcerptRepository;
        this.commentsRepository = commentsRepository;
        this.attachmentRepository = attachmentRepository;
        this.userRepository = userRepository;
    }

    @Override
    public DataSummaryResponseDto createDataSummary() {
        DataSummaryResponseDto dataSummaryDto = new DataSummaryResponseDto();

        dataSummaryDto.setNumberOfIncidents(incidentExcerptRepository.getItemsCount());
        dataSummaryDto.setNumberOfComments(commentsRepository.getItemsCount());
        dataSummaryDto.setNumberOfAttachments(attachmentRepository.getItemsCount());
        dataSummaryDto.setNumberOfUsers(userRepository.getItemsCount());

        Map<String, Integer> incidentTypesMap = new HashMap<>();
        for (IncidentType incidentType : IncidentType.values()) {
            incidentTypesMap.put(incidentType.getValue(), incidentExcerptRepository.getItemsCountByType(incidentType));
        }
        dataSummaryDto.setIncidentsByType(incidentTypesMap);

        Map<String, Integer> incidentVisibilityMap = new HashMap<>();
        Map<String, Integer> commentsVisibilityMap = new HashMap<>();
        for (Visibility visibility : Visibility.values()) {
            incidentVisibilityMap
                    .put(visibility.getValue(), incidentExcerptRepository.getItemsCountByVisibility(visibility));
            commentsVisibilityMap.put(visibility.getValue(), commentsRepository.getItemsCountByVisibility(visibility));
        }
        dataSummaryDto.setIncidentsByVisibility(incidentVisibilityMap);
        dataSummaryDto.setCommentsByVisibility(commentsVisibilityMap);

        return dataSummaryDto;
    }
}
