/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services.impl;

import org.eclipse.rdf4j.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sk.opendata.mapazlocinu.dto.CreateItemResponseDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentAdminDto;
import sk.opendata.mapazlocinu.dto.attachment.AttachmentPublicDto;
import sk.opendata.mapazlocinu.dto.attachment.UpdateAttachmentRequestDto;
import sk.opendata.mapazlocinu.dto.enums.CreateResponseItemType;
import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.incident.IncidentAdminDto;
import sk.opendata.mapazlocinu.entity.Attachment;
import sk.opendata.mapazlocinu.entity.Incident;
import sk.opendata.mapazlocinu.entity.IncidentFull;
import sk.opendata.mapazlocinu.exception.AttachmentException;
import sk.opendata.mapazlocinu.repository.AttachmentRepository;
import sk.opendata.mapazlocinu.repository.IncidentFullRepository;
import sk.opendata.mapazlocinu.repository.IncidentRepository;
import sk.opendata.mapazlocinu.services.AttachmentService;
import sk.opendata.mapazlocinu.services.PropertiesService;
import sk.opendata.mapazlocinu.utils.ObjectsMapper;
import sk.opendata.mapazlocinu.utils.Utils;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AttachmentServiceImpl implements AttachmentService {
    private final IncidentFullRepository incidentFullRepository;
    private final IncidentRepository incidentRepository;
    private final AttachmentRepository attachmentRepository;
    private final PropertiesService propertiesService;
    private final ObjectsMapper objectsMapper;

    private String UPLOAD_DIR;
    private int MAX_UPLOAD_TIME_IN_SEC;

    @Autowired
    public AttachmentServiceImpl(IncidentFullRepository incidentFullRepository,
            IncidentRepository incidentRepository,
            AttachmentRepository attachmentRepository,
            PropertiesService propertiesService, ObjectsMapper objectsMapper) {
        this.incidentFullRepository = incidentFullRepository;
        this.incidentRepository = incidentRepository;
        this.attachmentRepository = attachmentRepository;
        this.propertiesService = propertiesService;
        this.objectsMapper = objectsMapper;
    }

    @Override
    public CreateItemResponseDto createAttachment(String incidentId, MultipartFile multipartFile) throws IOException {
        IncidentFull incident = getNotNullIncidentOrThrowEx(incidentId);
        canCreateAttachmentForIncident(incident);

        Attachment attachment = createAttachmentEntity(multipartFile);

        saveFile(multipartFile, attachment);
        incident.setAttachments(addAttachmentToNotNullList(incident, attachment));
        incidentFullRepository.update(incident);

        return new CreateItemResponseDto(attachment.getId(), Visibility.valueOf(incident.getVisibility()),
                CreateResponseItemType.ATTACHMENT);
    }

    @Override
    public List<AttachmentPublicDto> getApprovedIncidentAttachments(String incidentId,
            long maxSize,
            Comparator<Attachment> comparator) {

        List<Attachment> attachments = getNotNullApprovedIncidentOrThrowEx(incidentId).getAttachments();
        if (attachments == null) {
            return new ArrayList<>();
        }

        List<AttachmentPublicDto> publicAttachments = objectsMapper.toPublicAttachmentsListDto(
                attachments.stream()
                        .limit(maxSize)
                        .sorted(comparator)
                        .collect(Collectors.toList()));
        publicAttachments.forEach(item -> item.toPublicAttachmentResource(incidentId));
        return publicAttachments;
    }

    @Override
    public Attachment getApprovedIncidentAttachment(String incidentId, String attachmentId) {
        getNotNullApprovedIncidentOrThrowEx(incidentId);    // just for check if incident exists and is approved
        return getNotNullAttachmentOrThrowEx(attachmentId);
    }

    @Override
    public IncidentAdminDto getIncidentForAttachment(String id) {
        getNotNullAttachmentOrThrowEx(id);  // just for check if attachment exists
        Resource incidentResource = attachmentRepository.getIncidentIdForAttachment(id);

        if (incidentResource != null) {
            Incident incident = incidentRepository.findById(incidentResource);
            return incident != null ? objectsMapper.toAdminIncidentDto(incident) : null;
        }
        return null;
    }

    @Override
    public FileSystemResource getAttachmentData(Attachment attachment) {
        return getFileContent(getAttachmentPathString(attachment));
    }

    @Override
    public Attachment updateAttachment(String id, UpdateAttachmentRequestDto updateAttachment) throws IOException {
        Attachment attachment = getNotNullAttachmentOrThrowEx(id);
        String previousTitle = attachment.getTitle();

        if (!previousTitle.equals(updateAttachment.getTitle())) {
            renameFile(attachment, updateAttachment.getTitle());
        }

        attachment.setCreatedDate(updateAttachment.getCreatedDate());
        attachmentRepository.update(attachment);

        return attachment;
    }

    @Override
    public void deleteAttachment(String id) throws IOException {
        Attachment attachment = getNotNullAttachmentOrThrowEx(id);
        Resource incidentResource = attachmentRepository.getIncidentIdForAttachment(id);
        if (incidentResource == null) {
            throw new EntityNotFoundException(
                    String.format("Corresponding incident to attachment with ID %s was not found.", id));
        }
        IncidentFull incident = getNotNullIncidentOrThrowEx(incidentResource);

        List<Attachment> attachments = incident.getAttachments();
        if (attachments == null) {
            return;
        }

        attachments.remove(attachment);
        incident.setAttachments(attachments);

        incidentFullRepository.update(incident);
        deleteFile(attachment);
        attachmentRepository.delete(id);
    }

    @Override
    public Attachment getAttachment(String id) {
        return getNotNullAttachmentOrThrowEx(id);
    }

    @Override
    public AttachmentAdminDto getAttachmentWithLinks(String id) {
        return objectsMapper.toAdminAttachmentDto(getAttachment(id));
    }

    @Override
    public List<AttachmentAdminDto> getIncidentAttachments(String incidentId,
            long maxSize,
            Comparator<Attachment> comparator) {

        List<Attachment> attachments = getNotNullIncidentOrThrowEx(incidentId).getAttachments();
        if (attachments == null) {
            return new ArrayList<>();
        }

        return objectsMapper.toAdminAttachmentsListDto(
                attachments.stream()
                        .limit(maxSize)
                        .sorted(comparator)
                        .collect(Collectors.toList()));
    }

    @Override
    public List<AttachmentAdminDto> getAllAttachments(long maxSize, Comparator<Attachment> comparator) {
        return objectsMapper.toAdminAttachmentsListDto(
                attachmentRepository.findAll().stream()
                        .limit(maxSize)
                        .sorted(comparator)
                        .collect(Collectors.toList()));
    }

    private Attachment createAttachmentEntity(MultipartFile file) {
        Attachment attachment = new Attachment();
        String filename = file.getOriginalFilename();
        String name = filename;
        String ext = "";

        int lastDot = filename.lastIndexOf('.');
        if (lastDot != -1 && lastDot != 0) {
            name = filename.substring(0, lastDot);
            ext = filename.substring(lastDot);
        }

        attachment.setTitle(name);
        attachment.setExtension(ext);
        attachment.setContentType(file.getContentType());
        attachment.setSize(file.getSize());

        attachment.setId(Utils.getDateFormattedId() + propertiesService.getIdForAttachmentAndIncrement());
        attachment.setCreatedDate(new Date());

        return attachment;
    }

    private IncidentFull getNotNullApprovedIncidentOrThrowEx(String incidentId) {
        IncidentFull incident = incidentFullRepository.findById(incidentId);
        if (incident == null || !incident.getVisibility().equals(Visibility.APPROVED.getValue())) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", incidentId));
        }

        return incident;
    }

    private IncidentFull getNotNullIncidentOrThrowEx(String incidentId) {
        IncidentFull incident = incidentFullRepository.findById(incidentId);
        if (incident == null) {
            throw new EntityNotFoundException(String.format("Incident with ID %s was not found.", incidentId));
        }

        return incident;
    }

    private IncidentFull getNotNullIncidentOrThrowEx(Resource incidentResource) {
        IncidentFull incident = incidentFullRepository.findById(incidentResource);
        if (incident == null) {
            throw new EntityNotFoundException(
                    String.format("Incident with ID %s was not found.", incidentResource.stringValue()));
        }

        return incident;
    }

    private Attachment getNotNullAttachmentOrThrowEx(String attachmentId) {
        Attachment attachment = attachmentRepository.findById(attachmentId);
        if (attachment == null) {
            throw new EntityNotFoundException(String.format("Attachment with ID %s was not found.", attachmentId));
        }

        return attachment;
    }

    private void saveFile(MultipartFile multipartFile, Attachment attachment) throws IOException {
        File uploadFolder = new File(UPLOAD_DIR);
        if (attachment.getSize() > uploadFolder.getFreeSpace()) {
            throw new AttachmentException("Not enough space to store file on the server!");
        }
        if (!isImageFile(multipartFile.getContentType())) {
            throw new AttachmentException("Unsupported file type! Supported are only images.");
        }

        Files.write(getAttachmentPath(attachment), multipartFile.getBytes());
    }

    private FileSystemResource getFileContent(String filePath) {
        return new FileSystemResource(new File(filePath));
    }

    private void deleteFile(Attachment attachment) throws IOException {
        Files.delete(getAttachmentPath(attachment));
    }

    private void renameFile(Attachment attachment, String newTitle) throws IOException {
        Path oldPath = getAttachmentPath(attachment);
        attachment.setTitle(newTitle);
        Path newPath = getAttachmentPath(attachment);

        Files.move(oldPath, newPath, StandardCopyOption.REPLACE_EXISTING);
    }

    private Path getAttachmentPath(Attachment a) {
        return Paths.get(getAttachmentPathString(a));
    }

    private String getAttachmentPathString(Attachment a) {
        return UPLOAD_DIR + "\\" + a.getId() + "-" + a.getTitle() + a.getExtension();
    }

    private List<Attachment> addAttachmentToNotNullList(IncidentFull incident, Attachment attachment) {
        List<Attachment> attachmentList =
                incident.getAttachments() != null ? incident.getAttachments() : new ArrayList<>();
        attachmentList.add(attachment);

        return attachmentList;
    }

    private boolean isImageFile(String contentType) {
        return contentType.startsWith("image/");
    }

    private void canCreateAttachmentForIncident(Incident incident) throws AttachmentException {
        Calendar incidentsTolerance = Calendar.getInstance();
        incidentsTolerance.setTime(incident.getCreatedDate());
        incidentsTolerance.add(Calendar.SECOND, MAX_UPLOAD_TIME_IN_SEC);

        if (incidentsTolerance.before(Calendar.getInstance())) {
            throw new AttachmentException(
                    String.format("Attachment for incident %s can not be created!", incident.getId()));
        }
    }

    @Value("${incidents.upload-folder}")
    public void setUPLOAD_DIR(String UPLOAD_DIR) {
        this.UPLOAD_DIR = UPLOAD_DIR;
    }

    @Value("${incidents.upload-time-in-sec}")
    public void setMAX_UPLOAD_TIME_IN_SEC(String MAX_UPLOAD_TIME_IN_SEC) {
        this.MAX_UPLOAD_TIME_IN_SEC = Integer.parseInt(MAX_UPLOAD_TIME_IN_SEC);
    }
}
