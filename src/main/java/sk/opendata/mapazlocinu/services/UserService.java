/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (server part).
 *
 * Crime Map - Crowd-sourcing solution (server part) is free software: you
 * can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (server part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Crime Map - Crowd-sourcing solution (server part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.services;

import sk.opendata.mapazlocinu.dto.enums.Visibility;
import sk.opendata.mapazlocinu.dto.user.UpdateUserRequestDto;
import sk.opendata.mapazlocinu.dto.user.UserAdminLinksDto;
import sk.opendata.mapazlocinu.dto.user.UserItemsAuthorResponseDto;
import sk.opendata.mapazlocinu.entity.User;

import java.util.Comparator;
import java.util.List;

public interface UserService {
    Visibility getRatingBasedVisibilityForUser(int rating);

    User getUserOrCreate(String id, String name);

    User createUser(String id, String name);

    void updateScoreForUser(User user, Visibility input, Visibility output);

    void updateScoreForUser(User user, String input, Visibility output);

    /* Methods for ADMIN API */
    List<UserAdminLinksDto> getAllUsers(long maxSize, Comparator<User> comparator);

    UserAdminLinksDto updateUser(String id, UpdateUserRequestDto userUpdate);

    void deleteUser(String id);

    UserAdminLinksDto getUser(String id);

    UserItemsAuthorResponseDto getUserAuthorItems(String id);
}
