﻿# Mapa Zločinu (Crime Map) Crowd-sourcing solution (Server part)

This repository containts server part of application used as Crowd-sourcing solution for project
[Crime map](http://mapazlocinu.opendata.sk/). It is the application with focus on collecting information
about various offenses and crimes from people. This server part of application provides API for Android 
client application which is in separated project.

## Compilation and run
This project is standard Spring Boot project build with Maven. To build this project just build it like
a standard Maven project or import it to your favourite IDE and run the project there. There can be one problem
with dependencies. It might be necessary to build [RDFBeans framework](https://github.com/cyberborean/rdfbeans) 
locally at your computer and install install it to your local Maven repository by running _mvn install_ command. This
project was build with RDFBeans framework version 2.2, so just for sure I've made fork of that version to my repository
[here](https://github.com/pavolgolias/rdfbeans).

After successful compilation you can run this project as standard Spring Boot project by running compiled JAR file
with included Tomcat. Do not forget to **set up the project** first (see the Settings section below). You will also need to 
run **RDF4J Server** (and RDF4J Workbench maybe too) to run your tripple store (database). Everything important can be 
found at [RDF4J docs website](http://docs.rdf4j.org/).

### Settings
To setup project just rename _**application.properties_default**_ to _**application.properties**_ and fill 
configuration according to yourself. 

Basic configuration items (with some example values) are:

| Setting | Example value | Description |
| ------------ | ------------- | ------------- |
| **server.port** | 8081 | server port number |
| **logging.level.sk.opendata.mapazlocinu** | debug | server logging level |
| **spring.http.multipart.max-file-size** | 6MB | max size of attachments |
| **spring.http.multipart.max-request-size** | 6MB | max size of attachments |
| **app.swagger.enable** | true | if true then Swagger API description will be available | 
| **rdf4j.server.url** | http://localhost:8080/rdf4j-server | URL of RDF4J Server |
| **rdf4j.server.repository-id** | first-repo | ID of RDF4J Server Repository |
| **incidents.upload-folder** | D:\\apache-tomcat-uploads | folder for storing attachments |
| **incidents.upload-time-in-sec** | 60 | number of seconds which means the time during which attachments can be added to the newly created incident |
| **users.rating.init** | 0 | init value of user score |
| **users.rating.increment** | 1 | increment value for user score |
| **users.rating.decrement** | 2 | decrement value for user score |
| **users.rating.approval-limit** | 5 | users' posts are automatically approved if user has score higher that this approval limit |
| **app.security.secret** | cm123cm456 | secret key used for signature of JWT authentication token |
| **app.security.token.expiration-time-in-mins** | 60 | validity of authentication token in minutes |
| **app.security.admin.init** | true | if true then new administrator user with credentials described below will be created and stored |
| **app.security.admin.username** | cmadmin | login for new admin |
| **app.security.admin.password** | pass123 | password for new admin |

## Licence
Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)

This file is part of Crime Map - Crowd-sourcing solution (server part).

Crime Map - Crowd-sourcing solution (server part) is free software: you
can redistribute it and/or modify it under the terms of the
GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Crime Map - Crowd-sourcing solution (server part) is distributed
in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Crime Map - Crowd-sourcing solution (server part).
If not, see <http://www.gnu.org/licenses/>.

This project uses following 3rd party components and libraries:
- Spring Boot [Apache 2.0 license]
- Java JWT [Apache 2.0 license]
- RDF4J [EDL]
- RDFBeans [Apache 2.0 license]
- Apache Commons [Apache 2.0 license]
- Javax Persistence API [CDDL]
- Orika Core [Apache 2.0 license]
- Springfox Swagger 2 [Apache 2.0 license]
